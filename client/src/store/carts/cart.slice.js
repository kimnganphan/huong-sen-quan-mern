import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userId: "",
  cartId: "",
  cartProducts: [],
  cartQuantity: 0, // số lượng món đã chọn
  totalDishQuantity: 0, // tổng tất cả món ăn
  totalAmount: 0,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    getCartProducts: (state, action) => {
      state.cartId = action.payload.id;
      state.cartProducts = action.payload.products;
      state.totalDishQuantity = state.cartProducts.reduce((totalQuantity, prod)=>{
        return totalQuantity + prod.quantity
      },0)

      state.totalAmount = state.cartProducts.reduce((totalPrice, prod)=>{
        return totalPrice + (prod.quantity * prod.product.price)
      },0)
      state.cartQuantity = state.cartProducts.length;
    },
    // addItemToCart: (state, action) => {
    //   const product = state.cartProducts.find((item) => {
    //     return item.product._id === action.payload._id;
    //   });

    //   if (product) {
    //     product.quantity += action.payload.quantity;
    //     state.totalAmount += product.product.price * action.payload.quantity;
    //     state.totalDishQuantity += action.payload.quantity;
    //   } else {
    //     state.cartProducts.push(action.payload.product);
    //     state.totalAmount +=
    //       action.payload.product.product.price *
    //       action.payload.product.quantity;
    //     state.totalDishQuantity += action.payload.product.quantity;
    //   }

    //   state.cartQuantity = state.cartProducts.length;
    // },
    clearCartAfterSubmit: (state) => {
      state.cartProducts = [];
      state.cartQuantity = 0;
      state.totalDishQuantity = 0;
      state.totalAmount = 0;
    },
    deleteItemFromCart: (state, action) => {
      state.cartProducts.splice(
        state.cartProducts.findIndex(
          (item) => item.product._id === action.payload.id
        ),
        1
      );

      
      state.totalDishQuantity = state.cartProducts.reduce((totalQuantity, prod)=>{
        return totalQuantity + prod.quantity
      },0)

      state.totalAmount = state.cartProducts.reduce((totalPrice, prod)=>{
        return totalPrice + (prod.quantity * prod.product.price)
      },0)
      state.cartQuantity = state.cartProducts.length;
    },
    // editCartItemQuantity: (state, action) => {
    //   const product = state.cartProducts.find((item) => {
    //     return item.product._id === action.payload.id;
    //   });

    //   if (product) {
    //     if (action.payload.buttonType === "des") {
    //       if (product.quantity > 1) {
    //         product.quantity -= 1;
    //         state.totalAmount -= product.product.price;
    //         state.totalDishQuantity -= 1;
    //       }
    //     } else if (action.payload.buttonType === "inc") {
    //       product.quantity += 1;
    //       state.totalAmount += product.product.price;
    //       state.totalDishQuantity += 1;
    //     }
    //   }

    //   state.cartQuantity = state.cartProducts.length;
    // },
    clearCartAfterSignOut: (state) => {
      state.userId = "";
      state.cartId = "";
      state.cartProducts = [];
      state.cartQuantity = 0;
      state.totalDishQuantity = 0;
      state.totalAmount = 0;
    },
    getUserId: (state, action) => {
      state.userId = action.payload
    }
  },
});

export const {
  addItemToCart,
  deleteItemFromCart,
  clearCartAfterSubmit,
  editCartItemQuantity,
  getCartProducts,
  clearCartAfterSignOut,
  getUserId
} = cartSlice.actions;

export default cartSlice.reducer;
