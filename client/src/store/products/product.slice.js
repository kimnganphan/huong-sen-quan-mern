import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  product: {},
  products: [],
  paginationProducts: [],
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.products.push(action.payload);
    },
    updateProduct: (state, action) => {
      state.products[
        state.products.findIndex((item) => item._id === action.payload.id)
      ] = action.payload.product;
    },
    getProducts: (state, action) => {
      state.products = action.payload;
    },
    getPaginationProducts: (state, action) => {
      state.paginationProducts = action.payload;
    },
    deleteProduct: (state, action) => {
      state.products.splice(
        state.products.findIndex((item) => item._id === action.payload),
        1
      );
    },
    getSingleProduct: (state, action) => {
      state.product = action.payload;
    },
    relatedProduct: (state, action) => {
      const result = state.products.filter((item) => {
        return (item._id !== action.payload.id && item.categories.includes(action.payload.categories[0]))
      });
      state.paginationProducts = result.slice(0,3)
    },
  },
});

export const {
  addProduct,
  updateProduct,
  getProducts,
  getPaginationProducts,
  deleteProduct,
  getSingleProduct,
  relatedProduct
} = productSlice.actions;

export default productSlice.reducer;
