import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
  username: "",
  email: "",
  lastName: "",
  firstName: "",
  phoneNumber: "",
  address: "",
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loginSuccess: (state, action) => {
      state.user = action.payload;
    },
    signOutSuccess: (state) => {
      state.user = null;
    },
    updateUser: (state, action) => {
      state.user = action.payload;
    },
    defaultUsername: (state) => {
      state.username = state.user.username;
    },
    setUsername: (state, action) => {
      state.username = action.payload;
    },
    defaultEmail: (state) => {
      state.email = state.user.email;
    },
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    defaultLastName: (state) => {
      state.lastName = state.user.lastName;
    },
    setLastName: (state, action) => {
      state.lastName = action.payload;
    },
    defaultFirstName: (state) => {
      state.firstName = state.user.firstName;
    },
    setFirstName: (state, action) => {
      state.firstName = action.payload;
    },
    defaultPhoneNumber: (state) => {
      if (state.user.phoneNumber === undefined || state.user.phoneNumber === null) {
        state.phoneNumber = "";
      } else {
        state.phoneNumber = state.user.phoneNumber;
      }
    },
    setPhoneNumber: (state, action) => {
      state.phoneNumber = action.payload;
    },
    defaultAddress: (state) => {
      if (state.user.address === undefined) {
        state.address = "";
      } else {
        state.address = state.user.address;
      }
    },
    setAddress: (state, action) => {
      state.address = action.payload;
    },
  },
});

export const {
  loginSuccess,
  signOutSuccess,

  updateUser,
  defaultUsername,
  setUsername,
  defaultEmail,
  setEmail,
  defaultLastName,
  setLastName,
  defaultFirstName,
  setFirstName,
  defaultPhoneNumber,
  setPhoneNumber,
  defaultAddress,
  setAddress
} = userSlice.actions;

export default userSlice.reducer;
