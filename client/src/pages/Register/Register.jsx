import { useState } from "react";
import { Link } from "react-router-dom";
import "./Register.scss";
import { SmallModal } from "../../components/Modal";
import { Button } from "../../components/Button";
import { registerUser } from "../../api";
const Register = () => {
  const [input, setInput] = useState({});
  //MODAL
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState("");
  const [title, setTitle] = useState("");

  //HANDLE INPUT EVENT ONCHANGE
  const handleRegisterInput = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  //VALIDATE EMAIL PATTERN
  const validateEmailPattern = () => {
    const emailFormat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (input.email) {
      if (!emailFormat.test(input.email)) {
        return false;
      } else {
        return true;
      }
    }
  };

  //VALIDATE REGISTER INFO
  const validateRegisterInfo = () => {
    //VALIDATE LAST NAME
    if (!input.lastName || input.lastName.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Họ");
      setTitle("Cảnh Báo Thiếu Dữ Liệu");
      return false;
    }
    //VALIDATE FIRST NAME
    if (!input.firstName || input.firstName.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Tên");
      setTitle("Cảnh Báo Thiếu Dữ Liệu");
      return false;
    }

    //VALIDATE USERNAME
    if (!input.username || input.username.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Username");
      setTitle("Cảnh Báo Thiếu Dữ Liệu");
      return false;
    }

    //VALIDATE EMAIL
    if (!input.email || input.email.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Email");
      setTitle("Cảnh Báo Thiếu Dữ Liệu");
      return false;
    }
    if (!validateEmailPattern()) {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Đúng Định Dạng Email");
      setTitle("Cảnh Báo Thiếu Dữ Liệu");
      return false;
    }
    //VALIDATE PASSWORD
    if (!input.password || input.password.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Mật Khẩu");
      setTitle("Cảnh Báo Thiếu Dữ Liệu");
      return false;
    }
    return true;
  };

  //HANDLE REGISTER CLICK EVENT
  const handleRegisterClick = () => {
    const isValidated = validateRegisterInfo();

    if (isValidated) {
      registerUser(input, setInput, setIsVisible, setTitle, setContent);
    }
  };
  return (
    <div className="register">
      <div className="register-wrapper">
        {/* ----- LAST NAME ----- */}
        <input
          type="text"
          placeholder="Họ"
          name="lastName"
          value={input.lastName ? input.lastName : ""}
          onChange={handleRegisterInput}
        />
        {/* -----END OF LAST NAME ----- */}

        {/* ----- FIRST NAME ----- */}
        <input
          type="text"
          placeholder="Tên"
          name="firstName"
          value={input.firstName ? input.firstName : ""}
          onChange={handleRegisterInput}
        />
        {/* ----- END OF FIRST NAME ----- */}

        {/* --------- USERNAME ------ */}
        <input
          type="text"
          placeholder="Username"
          name="username"
          value={input.username ? input.username : ""}
          onChange={handleRegisterInput}
        />
        {/* ---------END OF USERNAME ------ */}

        {/* --------- EMAIL ------ */}
        <input
          type="text"
          placeholder="Email"
          name="email"
          value={input.email ? input.email : ""}
          onChange={handleRegisterInput}
        />
        {/* --------- END OF EMAIL ------ */}

        {/* --------- PASSWORD --------- */}
        <input
          type="password"
          placeholder="Mật Khẩu"
          name="password"
          value={input.password ? input.password : ""}
          onChange={handleRegisterInput}
        />
        {/* ---------END OF PASSWORD --------- */}

        {/* --------- REGISTER BUTTON --------- */}
        <button onClick={handleRegisterClick}>Đăng Kí</button>
        {/* --------- END OF REGISTER BUTTON --------- */}

        {/* --------- ALREADY HAVE ACCOUNT --------- */}
        <p>
          Đã Có Tài Khoản? <Link to="/login">Đăng Nhập</Link>
        </p>
        {/* --------- END OF ALREADY HAVE ACCOUNT --------- */}
      </div>

      {/* ---------- ALERT MODAL ---------- */}
      <SmallModal
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        title={title}
        content={content}
        footer={
          <Button
            type="close"
            buttonTitle="Xác Nhận"
            buttonType="confirm"
            onClose={() => setIsVisible(false)}
          />
        }
      />
      {/* ----------END OF ALERT MODAL ---------- */}
    </div>
  );
};

export default Register;
