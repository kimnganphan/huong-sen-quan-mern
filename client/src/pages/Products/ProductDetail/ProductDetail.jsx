import "./ProductDetail.scss";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { PageTitle } from "../../../components/Title";
import { useDispatch, useSelector } from "react-redux";
import {
  getSingleProductByProductId,
} from "../../../api/products/productApi";
import { numberWithCommas } from "../../../utils/currency/currency.helper";
import { changeCategories } from "./ProductDetail.helper";
import { icons } from "../../../assets/icons";
import { Button, onAddProductToCartClick } from "../../../components/Button";
import { Products } from "../../../components/ProductList";
const ProductDetail = () => {
  const id = useParams();
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product.product);
  const cart = useSelector((state) => state.cart);
  const [quantity, setQuantity] = useState(1);
  const [total, setTotal] = useState(0);

  //GET PRODUCT INFO FROM ID
  useEffect(() => {
    getSingleProductByProductId(id.productId, dispatch);
  }, [id.productId, dispatch]);

  //HANDLE QUANTITY
  const handleQuantity = (type) => {
    if (type === "des") {
      if (quantity > 1) {
        setQuantity((prev) => prev - 1);
      }
    } else if (type === "inc") {
      setQuantity((prev) => prev + 1);
    }
  };

  //HANDLE INPUT CHANGE EVENT
  const handleInputChange = (e) => {
    setQuantity(() => {
      if (isNaN(e.target.value)) {
        return 1;
      } else {
        return Number(e.target.value);
      }
    });
  };

  //SET TOTAL PRICE BASED ON QUANTITY AND PRODUCT PRICE
  useEffect(() => {
    setTotal(quantity * product.price);

    if (quantity < 1) {
      setQuantity(1);
    }
  }, [quantity, product.price]);

  return (
    <div className="product-detail">
      <div className="product-detail-wrapper">
        {/* ---------- PAGE TITLE ----------- */}
        <PageTitle content="Chi Tiết Sản Phẩm" />
        {/* ----------END OF PAGE TITLE ----------- */}

        {/* ---------- PRODUCT CONTENT ----------- */}
        <div className="product-detail-content">
          {/* -------- PRODUCT IMAGE ------- */}
          <div className="product-detail-contentLeft">
            <div className="pd-mainImage">
              <img src={product.img} alt={product.title} />
            </div>
          </div>
          {/* --------END OF PRODUCT IMAGE ------- */}

          {/* -------- PRODUCT INFOMATION ------- */}
          <div className="product-detail-contentRight">
            <div className="pd-title">{product.title?.toUpperCase()}</div>

            <div className="pd-price">
              {product.price && numberWithCommas(product.price)} VNĐ
            </div>

            <div className="pd-categories">
              {product.categories?.map((type) => {
                return <span key={type}>{changeCategories(type)}</span>;
              })}
            </div>

            <div className="pd-quantity">
              <button onClick={() => handleQuantity("des")}>
                {icons.minus}
              </button>
              <input
                type="text"
                value={quantity}
                onChange={(e) => handleInputChange(e)}
              />
              <button onClick={() => handleQuantity("inc")}>
                {icons.plus}
              </button>
            </div>

            <div className="pd-total">
              <p>
                Số Lượng Món Ăn: <span>{quantity}</span>{" "}
              </p>
              <p>
                Tổng Số Tiền: <span>{numberWithCommas(total)}</span> VNĐ
              </p>
            </div>

            <div className="pd-addCart">
              <Button
                buttonType="light"
                buttonTitle="Thêm Vào Giỏ Hàng"
                handleButtonClick={() =>
                  onAddProductToCartClick({
                    cartId: cart.userId,
                    product: product,
                    quantity: parseInt(quantity, 10),
                    dispatch,
                    setQuantity,
                  })
                }
              />
            </div>
          </div>
        </div>
        {/* ----------END OF PRODUCT CONTENT --------- */}

        {/* ------ RELATED PRODUCTS -------- */}
        <div className="product-detail-related">
          <div className="related-title">Các Sản Phẩm Bạn Có Thể Thích</div>
          <Products />
        </div>
        {/* ------END OF RELATED PRODUCTS -------- */}
      </div>
    </div>
  );
};

export default ProductDetail;
