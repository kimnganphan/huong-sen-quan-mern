import { useState } from "react";
import { Button } from "../../../../../components/Button";
import "./FilterProduct.scss";

const FilterProduct = ({ setCat, setTitle, setPage }) => {
  const [name, setName] = useState("");
  const [clicked, setClicked] = useState([]);

  const productType = [
    "north",
    "south",
    "middle",
    "dry",
    "noddle",
    "vegan",
    "drink",
    "dessert",
  ];
  const renderProductType = [
    "Đặc Sản miền Bắc",
    "Đặc Sản miền Nam",
    "Đặc Sản miền Trung",
    "Đồ Khô",
    "Đồ Nước",
    "Đồ Chay",
    "Đồ Uống",
    "Tráng Miệng",
  ];

  //HANDLE FILTER BUTTON CLICK EVENT
  const handleFilterClick = () => {
    setTitle(name.toLowerCase());
    setPage(1);
    setCat(clicked);
  };

  //HANDLE CATEGORIES CHECKBOX ONCHANGE
  const handleCatFilter = ({ type, index }) => {
    setClicked((prev) => {
      const isChecked = clicked.includes(type);

      if (isChecked) {
        return clicked.filter((item) => item !== type);
      } else {
        return [...prev, type];
      }
    });
  };

  return (
    <div className="filterProduct">
      {/* ---- TITLE ---- */}
      <div className="filterProduct-title">Tìm Kiếm Món Ăn</div>
      {/* ----- END OF TITLE ----- */}

      {/* ---- CONTENT ---- */}
      <div className="filterProduct-content">
        <div className="filter-name">
          <label>Tên Món Ăn</label>
          <input
            type="text"
            placeholder="Nhập Tên Món Ăn..."
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className="filter-cat">
          <label>Loại Món Ăn</label>
          <div className="categories">
            {productType.map((type, index) => {
              return (
                <span
                  key={type}
                  onClick={() => handleCatFilter({ type, index })}
                  className={clicked.includes(type) ? "activeCat" : ""}
                >
                  {renderProductType[index]}
                </span>
              );
            })}
          </div>
        </div>
      </div>

      {/* ---- END OF CONTENT ----- */}

      {/* ----BUTTON ------ */}
      <div className="filterProduct-button">
        <Button
          buttonTitle={`Tìm Kiếm`}
          buttonType="light"
          handleButtonClick={handleFilterClick}
        />
      </div>
      {/* ----END OF BUTTON ------ */}
    </div>
  );
};

export default FilterProduct;
