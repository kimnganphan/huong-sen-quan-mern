import "./ProductList.scss";
import { PageTitle } from "../../../components/Title";
import { Products } from "../../../components/ProductList";
import FilterProduct from "./components/FilterProduct/FilterProduct";
import { Pagination } from "../../../components/Pagination";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProductsFromApi } from "../../../api";
import { ChangePageImg } from "../../../components/Image";
import { images } from "../../../assets/images";
import { useLocation } from "react-router-dom";
const ProductList = () => {
  const product = useSelector((state) => state.product);
  const dispatch = useDispatch();
  const [limit, setLimit] = useState(6);
  const [page, setPage] = useState(1);
  const [title, setTitle] = useState("");
  const [cat, setCat] = useState([]);

  const { state } = useLocation();
  //GET ALL PRODUCTS FOR PAGINATION COMPONENT
  useEffect(() => {
    if (!state) {
      getAllProductsFromApi(cat, title, 0, page, dispatch);
    } else {
      getAllProductsFromApi(
        cat.length > 0 ? cat : [state],
        title,
        0,
        page,
        dispatch
      );
    }
  }, [cat, title, limit, page, state, dispatch]);

  //GET  PRODUCTS LIMIT
  useEffect(() => {
    if (!state) {
      getAllProductsFromApi(cat, title, limit, page, dispatch);
    } else {
      getAllProductsFromApi(
        cat.length > 0 ? cat : [state],
        title,
        limit,
        page,
        dispatch
      );
    }
  }, [cat, title, limit, page, state, dispatch]);

  return (
    <div className="product-list">
      <div className="product-list-wrapper">
        <PageTitle content="THỰC ĐƠN" />
        <FilterProduct
          cat={cat}
          setCat={setCat}
          setTitle={setTitle}
          setPage={setPage}
        />
        <Products />
        <Pagination
          productArr={product.products}
          limit={limit}
          page={page}
          setPage={setPage}
        />
        <ChangePageImg
          pagePath="/contact"
          pageName="Liên Hệ"
          imgSrc={images.contact3}
        />
      </div>
    </div>
  );
};

export default ProductList;
