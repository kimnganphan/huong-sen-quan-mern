import "./About.scss";
import { PageTitle } from "../../components/Title";
import { images } from "../../assets/images";
import { ChangePageImg } from "../../components/Image";
const About = () => {
  return (
    <div className="about">
      <div className="about-title">
        <PageTitle content="VỀ CHÚNG TÔI" />
      </div>
      <div className="about-image"></div>

      <div className="about-content">
        {/* ----- SPACES ------- */}
        <div className="about-content-item">
          <div className="about-content-item-left">
            <img src={images.aboutSpace} alt="space" data-about-image />
          </div>
          <div className="about-content-item-right">
            <h3 data-about-title>Không Gian Quán</h3>
            <p data-about-desc>
              Không gian quán đơn giản, rộng rãi, mát mẻ và được bài trí tinh
              tế, tạo cảm giác thoải mái cho thực khách.{" "}
            </p>
          </div>
        </div>
        {/* -----END OF SPACES ------- */}

        {/* ----- MENU ------- */}
        <div className="about-content-item">
          <div className="about-content-item-left">
            <h3 data-about-title>Thực Đơn</h3>
            <p data-about-desc>
              Nơi đây hội tụ những món ăn ba miền được nấu theo khẩu vị từng
              vùng như: đùi dê quay lu, giò heo muối chiên, bò nướng ống tre,
              heo tộc nướng da giòn, gà ta hấp niêu Quê Nhà, gà Đông tảo 3 món…
              Những món ăn này không chỉ ngon miệng mà còn được bài trí đẹp mắt,
              hấp dẫn thực khách từ cái nhìn đầu tiên.{" "}
            </p>
          </div>
          <div className="about-content-item-right">
            <img src={images.aboutMenu} alt="space" data-about-image />
          </div>
        </div>
        {/* -----END OF MENU ------- */}

        {/* ---- CHANGE PAGE IMAGE ---- */}
        <ChangePageImg
          imgSrc={images.aboutChange}
          pageName={`Thực Đơn`}
          pagePath="/products"
        />
        {/* ---- END OF CHANGE PAGE IMAGE ---- */}
      </div>
    </div>
  );
};

export default About;
