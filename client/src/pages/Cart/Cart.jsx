import { useDispatch, useSelector } from "react-redux";
import { PageTitle } from "../../components/Title";
import "./Cart.scss";
import { numberWithCommas } from "../../utils/currency/currency.helper";
import { useState } from "react";

import { icons } from "../../assets/icons";
import { Link } from "react-router-dom";
import {
  deleteProductFromCart,
  updateProductInCart,
} from "../../api/carts/cartApi";
import { createOrder } from "../../api/order/orderApi";
import { LargeModal } from "../../components/Modal";
import { Button } from "../../components/Button";
const Cart = () => {
  //REDUX
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  //MODAL
  const [isVisibleLarge, setIsVisibleLarge] = useState(false);
  const [content, setContent] = useState("");

  //HANDLE DELETE PRODUCT TO CART
  const handleDelete = (e, id) => {
    deleteProductFromCart(cart.userId, id, dispatch);
  };

  //HANDLE ORDER BUTTON CLICK
  const handleOrderClick = () => {
    const isValidated = validatedCustomerInfo();
    if (isValidated) {
      createOrder(
        {
          user: cart.userId,
          cartProducts: cart.cartId,
          totalAmount: cart.totalAmount,
          address,
          phoneNumber,
        },
        dispatch,
        setIsVisibleLarge,
        setContent,
        cart.userId
      );
    }
  };

  //HANDLE INCREASE OR DESCREASE QUALITY CLICK
  const handleQuantityClick = (type, id) => {
    if (type === "desc") {
      updateProductInCart(cart.userId, type, id, dispatch);
    } else if (type === "inc") {
      updateProductInCart(cart.userId, type, id, dispatch);
    }
  };

  //VALIDATE CUSTOMER INFO BEFORE ORDER
  const validatedCustomerInfo = () => {
    if (address === "") {
      alert("Thiếu Địa Chỉ");
      return false;
    }
    if (phoneNumber === "") {
      alert("Thiếu Số Điện Thoại");
      return false;
    }
    return true;
  };
  return (
    <div className="cart">
      <div className="cart-wrapper">
        {/* ------- PAGE TITLE ------- */}
        <PageTitle content="Giỏ Hàng Của Bạn" />
        {/* -------END OF PAGE TITLE ------- */}
        {cart.cartProducts.length > 0 ? (
          <div className="cart-content">
            {/* ------- CART PRODUCTS ------- */}
            <div className="cart-products">
              {cart.cartProducts.map((product, index) => {
                return (
                  <div className="cart-items" key={index}>
                    {/* ------- PRODUCT IMAGE ------- */}
                    <div className="cart-image">
                      <img src={product.product.img} alt="" />
                    </div>
                    {/* -------END OF PRODUCT IMAGE ------- */}

                    {/* ------- PRODUCT NAME AND PRICE------- */}
                    <div className="cart-title">
                      <Link to={`/products/${product.product._id}`}>
                        <p>
                          {product.product.title &&
                            product.product.title.toUpperCase()}
                        </p>
                      </Link>

                      <p>{numberWithCommas(product.product.price)} VNĐ</p>
                    </div>
                    {/* ------- END OF PRODUCT NAME AND PRICE------- */}

                    {/* ------- PRODUCT QUANTITY------- */}
                    <div className="cart-quantity">
                      <button
                        onClick={() =>
                          handleQuantityClick("desc", product.product._id)
                        }
                      >
                        {icons.minus}
                      </button>
                      <span>{product.quantity}</span>
                      <button
                        onClick={() =>
                          handleQuantityClick("inc", product.product._id)
                        }
                      >
                        {icons.plus}
                      </button>
                    </div>
                    {/* ------- END OF PRODUCT QUANTITY------- */}

                    {/* ------- PRODUCT PRICE------- */}
                    <div className="cart-price">
                      {numberWithCommas(
                        product.product.price * product.quantity
                      )}{" "}
                      VNĐ
                    </div>
                    {/* ------- END OF PRODUCT PRICE------- */}

                    {/* ------- DELETE PRODUCT BUTTON------- */}
                    <div className="cart-delete">
                      <p onClick={(e) => handleDelete(e, product.product._id)}>
                        {icons.close}
                      </p>
                    </div>

                    {/* -------END OF DELETE PRODUCT BUTTON------- */}
                  </div>
                );
              })}
            </div>
            {/* -------END OF CART PRODUCTS ------- */}

            {/* ------- CART CHECKOUT ------- */}
            <div className="cart-total">
              <div className="cart-total-wrapper">
                <div className="total-title">ĐƠN HÀNG</div>

                <div className="total-content">
                  <div className="tc-item">
                    <p>Số Lượng Đơn Hàng: </p>
                    <p>{cart.cartQuantity}</p>
                  </div>
                  <div className="tc-item">
                    <p>Tổng Số Món Ăn: </p>
                    <p>{cart.totalDishQuantity}</p>
                  </div>
                  <div className="tc-item">
                    <p>Tổng Số Tiền: </p>
                    <p>{numberWithCommas(cart.totalAmount)} VNĐ</p>
                  </div>
                </div>
                {/* ----- USER ------ */}
                <div className="total-user">
                  <div className="tu-item">
                    <label>Địa Chỉ</label>
                    <input
                      type="text"
                      placeholder="Vui Lòng Nhập Địa Chỉ"
                      onChange={(e) => setAddress(e.target.value)}
                    />
                  </div>

                  <div className="tu-item">
                    <label>Số Điện Thoại</label>
                    <input
                      type="text"
                      placeholder="Vui Lòng Nhập Số Điện Thoại"
                      onChange={(e) => setPhoneNumber(Number(e.target.value))}
                    />
                  </div>
                </div>

                <div className="total-button">
                  <button onClick={handleOrderClick}>Đặt Hàng</button>
                </div>
              </div>
            </div>
            {/* -------END OF CART CHECKOUT ------- */}
          </div>
        ) : (
          <div className="empty-cart">
            <p>BẠN CHƯA CÓ GIỎ HÀNG, VUI LÒNG THÊM SẢN PHẨM</p>
            <Link to="/products">
              Đi tới thực đơn <span>{icons.arrowRight}</span>{" "}
            </Link>
          </div>
        )}
        {/* ------- CART CONTENT ------- */}

        {/* ------- END OF CART CONTENT ------- */}
      </div>

      <LargeModal
        isVisibleLarge={isVisibleLarge}
        onClose={() => setIsVisibleLarge(false)}
        title="ĐƠN HÀNG TẠO THÀNH CÔNG"
        content={content}
        footer={
          <Button
            buttonType={`green`}
            buttonTitle="Đóng"
            handleButtonClick={() => setIsVisibleLarge(false)}
          />
        }
      />
    </div>
  );
};

export default Cart;
