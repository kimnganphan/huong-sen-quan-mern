import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getOrderByUserId } from "../../api/order/orderApi";
import { PageTitle } from "../../components/Title";
import "./OrderInfo.scss";

const OrderInfo = () => {
  const user = useSelector((state) => state.user);
  const order = useSelector((state) => state.order);
  const dispatch = useDispatch();
  useEffect(() => {
    getOrderByUserId(user.user._id, dispatch);
  }, [user.user._id, dispatch]);


  return (
    <div className="orderInfo">
      {/* ----- PAGE TITLE ------ */}
      <PageTitle content="Thông Tin Đơn Hàng" />
      {/* ----- END OF PAGE TITLE ------ */}

      {/* ----- CONTENT ------- */}
      <div className="orderInfo-wrapper">
        {order.orders.map((order, index) => {
          return (
            <div className="orderInfo-item" key={order._id}>
              <div className="order">{index + 1}</div>

              <div className="order-id">
                <h3>Mã Đơn Hàng</h3>
                <p>{order._id}</p>
              </div>

              <div className="order-status">
                <h3>Trạng Thái</h3>
                <p>{order.status}</p>
              </div>
            </div>
          );
        })}
      </div>
      {/* -----END OF CONTENT ------- */}
    </div>
  );
};

export default OrderInfo;
