import { useCallback, useEffect, useState } from "react";
import { gallery } from "./GalleryImage.helper";
import "./GalleryImage.scss";
import { SmallModal } from "../../../../components/Modal";
import useScreenSizeChange from "../../../../utils/hooks/use-screen-size.hook";
const GalleryImage = () => {
  const [imageData, setImageData] = useState(gallery);
  //MODAL
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState("");
  //SCREEN SIZE CHANGE EVENT
  const handleMatches = useCallback((e) => {
    if (e.matches) {
      setImageData(gallery.slice(0, 6));
    } else {
      setImageData(gallery);
    }
  }, []);


  useScreenSizeChange(1024, handleMatches);
  //HANDLE GALLERY IMAGE CLICK
  const handleImageClick = (e, id) => {
    setIsVisible(true);
    setContent(<img src={e.target.src} alt="" className="sm-gallery-image" />);
  };
  return (
    <div className="gallery-image">
      {imageData.map((item) => {
        return (
          <img
            src={item.src}
            alt=""
            key={item.id}
            onClick={(e) => handleImageClick(e, item.id)}
          />
        );
      })}

      <SmallModal
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        title="Chi Tiết Ảnh"
        content={content}
      />
    </div>
  );
};

export default GalleryImage;
