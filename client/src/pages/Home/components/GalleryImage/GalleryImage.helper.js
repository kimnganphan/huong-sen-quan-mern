import { images } from "../../../../assets/images";

export const gallery = [
    {
        id:1,
        src: images.gal1
    },
    {
        id:2,
        src: images.gal2
    },
    {
        id:3,
        src: images.gal3
    },
    {
        id:4,
        src: images.gal4
    },
    {
        id:5,
        src: images.gal5
    },
    {
        id:6,
        src: images.gal6
    },
    {
        id:7,
        src: images.gal7
    },
    {
        id:8,
        src: images.gal8
    },
    {
        id:9,
        src: images.gal9
    }
]