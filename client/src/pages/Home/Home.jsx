import "./Home.scss";
import GalleryImage from "./components/GalleryImage/GalleryImage";
import { SectionTitle } from "../../components/Title";
import { icons } from "../../assets/icons";
import { Button, onViewMoreProduct } from "../../components/Button";

import { Products } from "../../components/ProductList";
import { useDispatch } from "react-redux";
import { getAllProductsFromApi } from "../../api";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate()
  const [limit, setLimit] = useState(6);
  const [page, setPage] = useState(1);
  const [title, setTitle] = useState("");
  const [cat, setCat] = useState([]);

  //GET LIMIT PRODUCT
  useEffect(() => {
    getAllProductsFromApi(cat, title, limit, page, dispatch);
  }, [cat, title, limit, page, dispatch]);

  return (
    <div className="home">
      {/* ----- HEADER IMAGE ----- */}
      <div className="home-header"></div>
      {/* ----- END OF HEADER IMAGE ----- */}

      {/* ---- MENU ----- */}
      <div className="home-menu">
        <div className="home-menu-wrapper">
          <SectionTitle
            title={`Thực Đơn Của Quán`}
            subTitle="Hương Sen Quán, Tinh Hoa Ẩm Thực Việt"
            icon={icons.star}
          />

          <div className="home-menu-card">
            <Products />
          </div>

          <Button buttonTitle={`Xem Thêm`} buttonType="dark" handleButtonClick={()=>onViewMoreProduct(navigate)}/>
        </div>
      </div>
      {/* ----- END OF MENU ------ */}

      {/* ----- FOOD GALLERY ----- */}
      <div className="home-gallery">
        <SectionTitle
          title={`Thư Viện Ảnh`}
          subTitle="Những khoảnh khắc tuyệt đẹp của món ăn được ghi lại"
          icon={icons.camera}
        />
        <GalleryImage />
      </div>
      {/* -----END OF FOOD GALLERY ----- */}
    </div>
  );
};

export default Home;
