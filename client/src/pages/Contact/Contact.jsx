import "./Contact.scss";
import { PageTitle } from "../../components/Title";
import { SmallModal } from "../../components/Modal";
import { Button } from "../../components/Button";
import { ChangePageImg } from "../../components/Image";

import { images } from "../../assets/images";
import { icons } from "../../assets/icons";

import { useState } from "react";
import emailjs from "emailjs-com";

const Contact = () => {
  //INPUT AND SEND EMAIL
  const [input, setInput] = useState({});
  const [success, setSuccess] = useState(false);
  //Modal
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState("");
  //HANDLE INPUT CHANGE
  const handleInpChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  //HANDLE SEND MESSAGE CLICK
  const handleSendMessage = (e) => {
    e.preventDefault();
    const isValidated = validateInfomation();
    if (isValidated) {
      emailjs
        .send("service_sljm8mu", "template_qbscbmn", input, "U066e3BC7j5hisgAE")
        .then(
          function (response) {
            console.log("SUCCESS!", response.status, response.text);
            setInput({});
            setSuccess(true);
          },
          function (error) {
            console.log("FAILED...", error);
          }
        );
    }
  };

  //VALIDATE SEND INFORMATION
  const validateInfomation = () => {
    if (!input.name || input.name.trim() === "") {
      setIsVisible(true);
      setContent("Xin Vui Lòng Nhập Họ Và Tên");
      return false;
    }
    if (!input.email || input.email.trim() === "") {
      setIsVisible(true);
      setContent("Xin Vui Lòng Nhập Email");
      return false;
    }
    if (!input.message || input.message.trim() === "") {
      setIsVisible(true);
      setContent("Xin Vui Lòng Nhập Nội Dung Liên Hệ");
      return false;
    }
    return true;
  };
  return (
    <div className="contact">
      <div className="contact-wrapper">
        <PageTitle content="LIÊN HỆ" />

        <div className="contact-content">
          {/* ----- FULLNAME ----- */}
          <div className="contact-content-item">
            <label>
              Họ Và Tên <span>*</span>
            </label>
            <input
              type="text"
              placeholder="Vui Lòng Nhập Họ Và Tên..."
              onChange={handleInpChange}
              name="name"
              value={input.name ? input.name : ""}
            />
          </div>
          {/* -----END OF FULLNAME ----- */}

          {/* ----- EMAIL ADDRESS ----- */}
          <div className="contact-content-item">
            <label>
              Email <span>*</span>
            </label>
            <input
              type="text"
              placeholder="Vui Lòng Nhập Email..."
              onChange={handleInpChange}
              name="email"
              value={input.email ? input.email : ""}
            />
          </div>
          {/* ----- END OF EMAIL ADDRESS ----- */}

          {/* ----- PHONE NUMBER ----- */}
          <div className="contact-content-item">
            <label>Số Điện Thoại</label>
            <input
              type="text"
              placeholder="Vui Lòng Nhập Số Điện Thoại..."
              onChange={handleInpChange}
              name="phone"
              value={input.phone ? input.phone : ""}
            />
          </div>
          {/* ----- END OF PHONE NUMBER ----- */}

          {/* ----- REASON ----- */}
          <div className="contact-content-item">
            <label>
              Nội Dung Liên Hệ <span>*</span>
            </label>
            <textarea
              cols="30"
              rows="10"
              placeholder="Vui Lòng Nhập Nội Dung..."
              onChange={handleInpChange}
              name="message"
              value={input.message ? input.message : ""}
            ></textarea>
          </div>
          {/* ----- END OF REASON ----- */}

          <div className="contact-content-button">
            <button onClick={handleSendMessage}> Gửi Thông Tin</button>
          </div>
          {success && (
            <div className="contact-content-success">
              {<p>{icons.thumbUp}Bạn Đã Gửi Mail Thành Công!</p>}
            </div>
          )}

          <ChangePageImg
            imgSrc={images.contact}
            pageName={`Về Chúng Tôi`}
            pagePath="/about"
          />
        </div>
      </div>

      {/* --------- ALERT MODAL ------ */}
      <SmallModal
        title="Cảnh Báo Thiếu Dữ Liệu"
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        content={content}
        footer={
          <Button
            buttonTitle="Xác Nhận"
            buttonType="confirm"
            type="close"
            onClose={() => setIsVisible(false)}
          />
        }
      />

      {/* --------- END OF ALERT MODAL ---------- */}
    </div>
  );
};

export default Contact;
