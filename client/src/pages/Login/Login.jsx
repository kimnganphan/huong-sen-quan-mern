import "./Login.scss";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginUser } from "../../api/users/userApi";
import { useRef, useState } from "react";
import { SmallModal } from "../../components/Modal";
const Login = () => {
  const titleRef = useRef();
  //REDUX
  const dispatch = useDispatch();
  //REACT-ROUTER-DOM
  const navigate = useNavigate();
  //INPUT
  const [input, setInput] = useState({});
  //MODAL
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState("");
  const [title, setTitle] = useState("");
 

  //HANDLE ONCHANGE INPUT USERNAME AND PASSWORD EVENT
  const handleSignInChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  //HANLDE SIGN IN CLICK
  const handleNormalSignInClick = () => {
    const isValidated = validateSignInData();
    if (isValidated) {
      loginUser(input, dispatch, navigate, setIsVisible, setContent);
    }
  };

  //VALIDATE USERNAME AND PASSWORD BEFORE SUBMIT
  const validateSignInData = () => {
    if (!input.username || input.username.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Username");
      setTitle("Thông Báo");
      return false;
    }
    if (!input.password || input.password.trim() === "") {
      setIsVisible(true);
      setContent("Vui Lòng Nhập Password");
      setTitle("Thông Báo");
      return false;
    }
    return true;
  };

  return (
    <div className="login">
      <div className="login-wrapper">
        {/* ----- LOGIN WITH USERNAME AND PASSWORD ----- */}

        <h3 ref={titleRef}>Đăng Nhập Vào Tài Khoản</h3>
        {/* ------ USERNAME ----- */}
        <input
          type="text"
          placeholder="Username"
          name="username"
          onChange={handleSignInChange}
        />
        {/* ------END OF USERNAME ----- */}

        {/* ------ PASSWORD ----- */}
        <input
          type="password"
          placeholder="Password"
          name="password"
          onChange={handleSignInChange}
        />
        {/* ------ END OF PASSWORD ----- */}

        {/* ------ SIGN IN BUTTON ----- */}
        <button onClick={handleNormalSignInClick}>Đăng Nhập</button>
        {/* ------ END OF SIGN IN BUTTON ----- */}

        {/* ------ NAVIGATE REGISTER ----- */}
        <p>
          Chưa có tài khoản? <Link to="/register">Đăng Kí</Link>
        </p>
        {/* ------ END OF NAVIGATE REGISTER ----- */}

        {/* -----END OF LOGIN WITH USERNAME AND PASSWORD ----- */}
      </div>

      {/* ----- ALERT MODAL ---- */}
      <SmallModal
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        content={content}
        title={title}
      />
      {/* -----END OF ALERT MODAL ---- */}
    </div>
  );
};

export default Login;
