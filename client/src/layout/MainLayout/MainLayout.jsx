import "./MainLayout.scss";
import Navbar from "./Navbar/Navbar";
import Footer from "./Footer/Footer";
const MainLayout = ({ children }) => {
  return (
    <div className="mainlayout">
      <Navbar />
      {children}
      <Footer />
    </div>
  );
};

export default MainLayout;
