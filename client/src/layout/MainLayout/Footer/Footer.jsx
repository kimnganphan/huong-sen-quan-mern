import "./Footer.scss";
import { icons } from "../../../assets/icons";
import { Link, useNavigate } from "react-router-dom";
import { handleBackToTop } from "../../../utils/events/event.helper";
const Footer = () => {
  const navigate = useNavigate();
  const handleCatClick = (type) => {
    switch (type) {
      case "all":
        navigate("/products");
        handleBackToTop();
        break;
      case "north":
        navigate("/products", { state: "north" });
        handleBackToTop();
        break;
      case "middle":
        navigate("/products", { state: "middle" });
        handleBackToTop();
        break;
      case "south":
        navigate("/products", { state: "south" });
        handleBackToTop();
        break;
      case "dry":
        navigate("/products", { state: "dry" });
        handleBackToTop();
        break;
      case "noodle":
        navigate("/products", { state: "noddle" });
        handleBackToTop();
        break;
      case "vegan":
        navigate("/products", { state: "vegan" });
        handleBackToTop();
        break;
      case "drink":
        navigate("/products", { state: "drink" });
        handleBackToTop();
        break;
      case "dessert":
        navigate(`/products`, {state: 'dessert'});
        handleBackToTop();
        break;
      default:
        break;
    }
  };
  return (
    <footer>
      <div className="f-wrapper">
        {/* ----- BACK TO TOP BUTTON ----- */}
        <div className="f-button">
          <button onClick={handleBackToTop}>Quay lại trên đầu trang</button>
        </div>
        {/* -----END OF BACK TO TOP BUTTON ----- */}

        {/* ----- MAIN CONTENT ----- */}
        <div className="f-content">
          <div className="f-content-left">
            {/* ------- PAGES ------ */}
            <div className="f-content-left-pages">
              <h4>Các Trang</h4>
              <Link to="/" onClick={handleBackToTop}>
                Trang Chủ
              </Link>
              <Link to="/about" onClick={handleBackToTop}>
                Về Chúng Tôi
              </Link>
              <Link to="/contact" onClick={handleBackToTop}>
                Liên Hệ
              </Link>
              <Link to="/register" onClick={handleBackToTop}>
                Đăng Kí
              </Link>
              <Link to="/login" onClick={handleBackToTop}>
                Đăng Nhập
              </Link>
              <Link to="/cart" onClick={handleBackToTop}>
                Giỏ Hàng
              </Link>
            </div>
            {/* ------- END OF PAGES ------ */}

            {/* ------- CATEGORIES ------ */}
            <div className="f-content-left-categories">
              <h4>Các Loại Món Ăn</h4>
              <span onClick={() => handleCatClick("all")}>Tất Cả Các Món</span>
              <span onClick={() => handleCatClick("north")}>
                Đặc Sản Miền Bắc
              </span>
              <span onClick={() => handleCatClick("middle")}>
                Đặc Sản Miền Trung
              </span>
              <span onClick={() => handleCatClick("south")}>
                Đặc Sản Miền Nam
              </span>
              <span onClick={() => handleCatClick("noodle")}>Món Nước</span>
              <span onClick={() => handleCatClick("dry")}>Món Khô</span>
              <span onClick={() => handleCatClick("vegan")}>Món Chay</span>
              <span onClick={() => handleCatClick("drink")}>Đồ Uống</span>
              <span onClick={() => handleCatClick("dessert")}>Tráng Miệng</span>
            </div>
            {/* -------END OF CATEGORIES ------ */}
          </div>
          <div className="f-content-right">
            {/* ------- LOGO --------- */}
            <div className="f-content-right-logo">
              <h1>
                Hương Sen <br /> Quán {icons.lotusIcon}
              </h1>
            </div>
            {/* -------END OF LOGO --------- */}

            {/* ------- SOCIALS --------- */}
            <div className="f-content-right-socials">
              <h5>Follow Us</h5>
              <div>
                <a
                  href="https://www.instagram.com
"
                >
                  {icons.insta}
                </a>
                <a href="https://www.facebook.com">{icons.facebook}</a>
                <a href="https://www.youtube.com">{icons.youtube}</a>
              </div>
            </div>
            {/* ------- END OF SOCIALS --------- */}
          </div>
        </div>

        {/* -----END OF MAIN CONTENT ----- */}

        {/* ------- COPYRIGHT --------- */}
        <div className="f-copyright">&copy; Huong Sen Quan 2022</div>

        {/* ------- END OF COPYRIGHT --------- */}
      </div>
    </footer>
  );
};

export default Footer;
