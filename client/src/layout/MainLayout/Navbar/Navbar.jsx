import "./Navbar.scss";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { icons } from "../../../assets/icons";
import { useCallback, useEffect, useRef, useState } from "react";
import { handleBackToTop } from "../../../utils/events/event.helper";
import { useDispatch, useSelector } from "react-redux";
import { images } from "../../../assets/images";
import { LargeModal, SmallModal } from "../../../components/Modal";
import { signOutSuccess } from "../../../store/users/user.slice";
import LargeModalContent from "./component/LargeModalContent/LargeModalContent";
import { Button } from "../../../components/Button";
import { clearCartAfterSignOut } from "../../../store/carts/cart.slice";
import useScreenSizeChange from "../../../utils/hooks/use-screen-size.hook";
import LargeModalFooter from "./component/LargeModalFooter/LargeModalFooter";
const Navbar = () => {
  const navigation = useRef();
  const navRight = useRef();
  const navWrapper = useRef();

  const navigate = useNavigate();

  const [scrollNav, setScrollNav] = useState("");
  const [activeLeftNav, setActiveLeftNav] = useState("");

  const [fontColor, setFontColor] = useState("");
  //Modal
  const [isVisible, setIsVisible] = useState(false);
  const [isVisibleLarge, setIsVisibleLarge] = useState(false);
  const [title, setTitle] = useState("");
  const [modalContentType, setModalContentType] = useState("");
  const [footer, setFooter] = useState("");

  //REACT-ROUTER-DOM
  const location = useLocation();

  //REDUX
  const cart = useSelector((state) => state.cart);
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  //HANDLE FONT COLOR DEPENDS ON LOCATION
  useEffect(() => {
    if (location.pathname === "/") {
      setFontColor("");
    } else {
      setFontColor("fontDark");
    }
  }, [location.pathname]);

  //WINDOW ONSCROLL EVENT
  useEffect(() => {
    //HANDLE WINDOW ON SCROLL CHANGE NAVBAR
    const handleWindowOnScroll = () => {
      if (window.scrollY > 0) {
        setScrollNav("activeNavbar");
      } else {
        setScrollNav("");
      }
    };
    window.addEventListener("scroll", handleWindowOnScroll);

    return () => {
      window.removeEventListener("scroll", handleWindowOnScroll);
    };
  }, []);

  //HANDLE OPEN AND CLOSE MENU WHEN RESPONSIVE
  const handleMenu = (type) => {
    if (type === "open") {
      setActiveLeftNav("activeNavLeft");
      //SET BODY CANNOT SCROLL WHEN MENU IS OPEN
      document.body.style.overflow = "hidden";
    } else if (type === "close") {
      setActiveLeftNav("");
      //SET BODY CAN SCROLL WHEN MENU IS CLOSE
      document.body.style.overflow = "unset";
    }
  };
  //ON ESCAPE KEY EVENT
  useEffect(() => {
    const handleKeyDownPress = ({ key }) => {
      if (key === "Escape") {
        setActiveLeftNav("");
      }
    };

    document.addEventListener("keydown", handleKeyDownPress);

    return () => {
      document.removeEventListener("keydown", handleKeyDownPress);
    };
  }, []);

  //SCREEN SIZE CHANGE EVENT
  const handleMatches = useCallback((e) => {
    if (e.matches) {
      navigation.current.appendChild(navRight.current);
    } else {
      navWrapper.current.appendChild(navRight.current);
    }
  }, []);

  useScreenSizeChange(700, handleMatches);
  //EVENT CLICK ON ITEMS ON NAVBAR LEFT NAVIGATION
  useEffect(() => {
    const handleNavigationClick = () => {
      document.body.style.overflow = "unset";
    };
    navigation.current.addEventListener("click", handleNavigationClick);
  }, []);

  //HANDLE USER SIGN OUT
  const handleSignOut = () => {
    dispatch(signOutSuccess());
    dispatch(clearCartAfterSignOut());
    setIsVisible(false);
  };

  //HANDLE USER VIEW DETAIL
  const handleViewUserDetail = () => {
    setTitle("Thông Tin Người Dùng");
    setIsVisibleLarge(true);
    setModalContentType("view");
    setFooter(
      <Button
        buttonTitle={`Đóng`}
        handleButtonClick={() => setIsVisibleLarge(false)}
        buttonType="green"
      />
    );
  };

  //HANDLE USER UPDATE DETAIL
  const handleUpdateUserDetail = () => {
    setTitle("Thay Đổi Thông Tin Người Dùng");
    setIsVisibleLarge(true);
    setModalContentType("edit");
    setFooter(<LargeModalFooter setIsVisibleLarge={setIsVisibleLarge} />);
  };

  //HANDLE ORDER STATUS
  const handleOrderStatus = () => {
    navigate("/order");
  };

  const handleUserImgClick = () =>{
    setIsVisible(true)
    document.body.style.overflow = "hidden";
  }

  const handleClose = () =>{
    setIsVisible(false)
    document.body.style.overflow = "unset";
  }
  return (
    <nav className={scrollNav}>
      <div ref={navWrapper} className="nav-wrapper">
        {/* ----- NAVBAR MENU ICON (ONLY APPEAR WHEN RESPONSIVE) ------ */}
        <div className="nav-menu-icon" onClick={() => handleMenu("open")}>
          {icons.menu}
        </div>
        {/* -----END OF NAVBAR MENU ICON ------ */}

        {/* ----- NAVBAR LEFT (PAGES) ------*/}
        <div
          className={`nav-left ${activeLeftNav}`}
          onClick={() => handleMenu("close")}
        >
          <div
            ref={navigation}
            className={`navigation ${fontColor}`}
            onClick={(e) => e.stopPropagation()}
          >
            <div
              className="navi-close-menu"
              onClick={() => handleMenu("close")}
            >
              {icons.close}
            </div>
            <Link to="/" onClick={handleBackToTop}>
              Trang Chủ
            </Link>
            <Link to="/products" onClick={handleBackToTop}>
              Thực Đơn
            </Link>
            <Link to="/about" onClick={handleBackToTop}>
              Về Chúng Tôi
            </Link>
            <Link to="/contact" onClick={handleBackToTop}>
              Liên Hệ
            </Link>
          </div>
        </div>
        {/* ----- END OF NAVBAR LEFT (PAGES) ------*/}

        {/* ----- NAVBAR CENTER (LOGO) ------*/}
        <div className="nav-center">
          <h1 className={`${fontColor}`}>
            Hương Sen <br /> Quán {icons.lotusIcon}
          </h1>
        </div>
        {/* ----- END OF NAVBAR CENTER (LOGO) ------*/}

        {/* ----- NAVBAR RIGHT ------*/}
        <div ref={navRight} className={`nav-right ${fontColor}`}>
          {user === null ? (
            <Link to="/login" onClick={handleBackToTop}>
              {icons.userIcon} Đăng Nhập
            </Link>
          ) : (
            <img
              src={!user.avatar ? images.noImg : user.avatar}
              alt="customer-avatar"
              className="nav-user"
              onClick={handleUserImgClick}
            />
          )}

          <Link to="/cart" className="nav-cart" onClick={handleBackToTop}>
            {icons.cartIcon} Giỏ Hàng <span>{cart.cartProducts.length}</span>
          </Link>
        </div>
        {/* -----END OF NAVBAR RIGHT ------*/}
      </div>

      <SmallModal
        isVisible={isVisible}
        onClose={handleClose}
        title={`Chào Mừng Khách Hàng ${user?.firstName.toUpperCase()}`}
        content={
          <div className="nav-user-menu">
            <p onClick={handleSignOut}>Đăng Xuất</p>
            <p onClick={handleViewUserDetail}>Xem Thông Tin</p>
            <p onClick={handleUpdateUserDetail}>Thay đổi thông tin</p>
            <p onClick={handleOrderStatus}>Tình Trạng Đơn Hàng</p>
          </div>
        }
      />

      <LargeModal
        isVisibleLarge={isVisibleLarge}
        onClose={() => setIsVisibleLarge(false)}
        title={title}
        content={
          <LargeModalContent
            type={modalContentType}
            handleUpdateUserDetail={handleUpdateUserDetail}
            handleViewUserDetail={handleViewUserDetail}
          />
        }
        footer={footer}
      />
    </nav>
  );
};

export default Navbar;
