import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { images } from "../../../../../assets/images";
import "./LargeModalContent.scss";
import {
  setUsername,
  defaultUsername,
  setEmail,
  defaultEmail,
  setLastName,
  defaultLastName,
  setFirstName,
  defaultFirstName,
  setPhoneNumber,
  defaultPhoneNumber,
  defaultAddress,
  setAddress,
} from "../../../../../store/users/user.slice";
import { Button } from "../../../../../components/Button";
const LargeModalContent = ({
  type,
  handleUpdateUserDetail,
  handleViewUserDetail,
}) => {
  //REDUX
  const user = useSelector((state) => state.user.user);
  const state = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(defaultUsername(user.username));
    dispatch(defaultEmail(user.email));
    dispatch(defaultLastName(user.lastName));
    dispatch(defaultFirstName(user.firstName));
    dispatch(defaultPhoneNumber(user.phoneNumber));
    dispatch(defaultAddress(user.address));
  }, [user, dispatch]);

  return (
    <div className="lm-content">
      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Ảnh Đại Diện : </label>
        </div>

        <div className="lmci-right">
          {type === "view" ? (
            <img src={user.avatar ? user.avatar : images.noImg} alt="" />
          ) : (
            <img src={user.avatar ? user.avatar : images.noImg} alt="" />
          )}
        </div>
      </div>

      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Username : </label>
        </div>
        <div className="lmci-right">
          {type === "view" ? (
            <p>{user.username}</p>
          ) : (
            <input
              type="text"
              value={state.username}
              name="username"
              onChange={(e) => dispatch(setUsername(e.target.value))}
            />
          )}
        </div>
      </div>

      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Email : </label>
        </div>
        <div className="lmci-right">
          {type === "view" ? (
            <p>{user.email}</p>
          ) : (
            <input
              type="text"
              value={state.email}
              name="email"
              onChange={(e) => dispatch(setEmail(e.target.value))}
            />
          )}
        </div>
      </div>

      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Họ : </label>
        </div>
        <div className="lmci-right">
          {type === "view" ? (
            <p>{user.lastName}</p>
          ) : (
            <input
              type="text"
              value={state.lastName}
              name="lastName"
              onChange={(e) => dispatch(setLastName(e.target.value))}
            />
          )}
        </div>
      </div>
      {/* ----- FIRST NAME ----- */}
      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Tên : </label>
        </div>
        <div className="lmci-right">
          {type === "view" ? (
            <p>{user.firstName}</p>
          ) : (
            <input
              type="text"
              value={state.firstName}
              name="firstName"
              onChange={(e) => dispatch(setFirstName(e.target.value))}
            />
          )}
        </div>
      </div>
      {/* ----- END OF FIRST NAME ----- */}

      {/* ----- PHONE NUMBER ----- */}
      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Số Điện Thoại : </label>
        </div>
        <div className="lmci-right">
          {type === "view" ? (
            <p>{user.phoneNumber ? user.phoneNumber : "Chưa Có"}</p>
          ) : (
            <input
              type="text"
              value={state.phoneNumber}
              name="phoneNumber"
              onChange={(e) => dispatch(setPhoneNumber(e.target.value))}
            />
          )}
        </div>
      </div>
      {/* -----END OF PHONE NUMBER ----- */}

      {/* ----- ADDRESS ----- */}
      <div className="lm-content-item">
        <div className="lmci-left">
          <label>Địa Chỉ : </label>
        </div>
        <div className="lmci-right">
          {type === "view" ? (
            <p>{user.address ? user.address : "Chưa Có"}</p>
          ) : (
            <input
              type="text"
              value={state.address}
              name="address"
              onChange={(e) => dispatch(setAddress(e.target.value))}
            />
          )}
        </div>
      </div>
      {/* ----- END OF ADDRESS ----- */}

      <div className="lm-content-buttons">
        {type === "view" ? (
          <Button
            buttonTitle={`Chỉnh Sửa Thông Tin`}
            buttonType="dark"
            handleButtonClick={handleUpdateUserDetail}
          />
        ) : (
          <Button
            buttonTitle={`Quay Lại Xem Thông Tin`}
            buttonType="dark"
            handleButtonClick={handleViewUserDetail}
          />
        )}
      </div>
    </div>
  );
};

export default LargeModalContent;
