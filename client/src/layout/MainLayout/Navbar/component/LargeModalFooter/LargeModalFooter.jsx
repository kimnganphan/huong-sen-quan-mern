import React, { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, onEditUserInfo } from "../../../../../components/Button";

const LargeModalFooter = ({ setIsVisibleLarge }) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const userObj = {
    username: user.username,
    email: user.email,
    lastName: user.lastName,
    firstName: user.firstName,
    phoneNumber: user.phoneNumber,
    address: user.address,
  };
  return (
    <Fragment>
      <Button
        buttonTitle={`Đóng`}
        buttonType="green"
        handleButtonClick={() => setIsVisibleLarge(false)}
      ></Button>
      <Button
        buttonTitle={`Xác Nhận Sửa Thông Tin`}
        buttonType="beige"
        handleButtonClick={() =>
          onEditUserInfo({
            user,
            userObj,
            setIsVisibleLarge,
            dispatch,
          })
        }
      />
    </Fragment>
  );
};

export default LargeModalFooter;
