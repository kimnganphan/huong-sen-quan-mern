import "./Header.scss";
import { Link, NavLink } from "react-router-dom";
import {useCallback, useRef, useState } from "react";
import { icons } from "../../../assets/icons";
import useScreenSizeChange from '../../../utils/hooks/use-screen-size.hook'
const Header = () => {
  const headerRef = useRef();
  const headNavRef = useRef();
  const buttonRef = useRef()
  const [activeOpenNav, setActiveOpenNav] = useState("");

  //HANDLE OPEN AND CLOSE MENU WHEN RESPONSIVE
  const handleMenuClick = (type) => {
    if (type === "open") {
      setActiveOpenNav("open");
    } else if (type === "close") {
      setActiveOpenNav("");
    }
  };


  //HANDLE SCREEN SIZE CHANGE EVENT
  const handleMatches = useCallback((e) => {
    if (e.matches) {
      headNavRef.current.appendChild(buttonRef.current);
    } else {
      headerRef.current.appendChild(buttonRef.current);
    }
  }, []);

  useScreenSizeChange(700, handleMatches);
  return (
    <div ref={headerRef} className="header">
      {/* ----- LOGO ---- */}
      <div className="header-logo">Hương Sen Quán</div>
      {/* -----END OF LOGO ---- */}
      <div className="header-menu">
        <p onClick={() => handleMenuClick("open")}>Menu</p>
      </div>
      {/* ----- NAV LINK ---- */}
      <div
        className={`header-nav ${activeOpenNav}`}
        onClick={() => handleMenuClick("close")}
      >
        <div ref={headNavRef} className="headerNav-wrapper" onClick={(e) => e.stopPropagation()}>
          <div
            className="head-nav-close"
            onClick={() => handleMenuClick("close")}
          >
            {icons.close}
          </div>
          <Link to="/">Trang Chủ</Link>
          <Link to="/about">Về Chúng Tôi</Link>
          <Link to="/contact">Liên Hệ</Link>
        </div>
      </div>
      {/* -----END OF NAV LINK ---- */}

      {/* ----- NAV BUTTON ---- */}
      <div ref={buttonRef} className="header-buttons">
        <NavLink
          to="/login"
          className={({ isActive }) => (isActive ? "activeLink" : undefined)}
        >
          Đăng Nhập
        </NavLink>
        <NavLink
          to="/register"
          className={({ isActive }) => (isActive ? "activeLink" : undefined)}
        >
          Đăng Kí
        </NavLink>
      </div>
      {/* -----END OF NAV BUTTON ---- */}
    </div>
  );
};

export default Header;
