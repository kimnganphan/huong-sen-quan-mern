import './SubLayout.scss'
import Header from './Header/Header'
const SubLayout = ({children}) => {
  return (
    <div className='sublayout'>
      <div className='overlay'></div>
      <Header/>
      {children}
    </div>
  )
}

export default SubLayout