export const images = {
    mainHeading: require('./images/heading.jpg'),
    contact: require('./images/contact-2.jpg'),
    aboutSpace: require('./images/about-1.jpg'),
    aboutMenu: require('./images/about-2.jpg'),
    aboutChange: require('./images/about-3.jpg'),
    gal1: require('./images/gallery/gal-1.jpg'),
    gal2: require('./images/gallery/gal-2.jpg'),
    gal3: require('./images/gallery/gal-3.jpg'),
    gal4: require('./images/gallery/gal-4.jpg'),
    gal5: require('./images/gallery/gal-6.jpg'),
    gal6: require('./images/gallery/gal-7.jpg'),
    gal7: require('./images/gallery/gal-8.jpg'),
    gal8: require('./images/gallery/gal-9.jpg'),
    gal9: require('./images/gallery/gal-10.jpg'),
    contact3: require('./images/contact-3.jpg'),
    noImg: require('./images/no-img.jpg')

}