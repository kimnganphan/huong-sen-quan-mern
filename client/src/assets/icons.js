import {
  AiOutlineShoppingCart,
  AiOutlineUser,
  AiOutlineInstagram,
  AiOutlineMenu,
  AiOutlineClose,
  AiOutlineArrowRight,
  AiOutlineMail,
  AiOutlineStar,
  AiOutlineDoubleLeft,
  AiOutlineDoubleRight,
  AiOutlinePlus,
  AiOutlineMinus
} from "react-icons/ai";
import { GiLotus } from "react-icons/gi";
import { FaFacebook, FaYoutube } from "react-icons/fa";
import {BsHandThumbsUp,BsFillCameraFill} from 'react-icons/bs'
import {IoIosArrowBack,IoIosArrowForward} from 'react-icons/io'
export const icons = {
  cartIcon: <AiOutlineShoppingCart />,
  userIcon: <AiOutlineUser />,
  lotusIcon: <GiLotus />,
  insta: <AiOutlineInstagram />,
  facebook: <FaFacebook />,
  youtube: <FaYoutube />,
  menu: <AiOutlineMenu />,
  close : <AiOutlineClose/>,
  arrowRight: <AiOutlineArrowRight/>,
  mail :<AiOutlineMail/>,
  thumbUp: <BsHandThumbsUp/>,
  camera : <BsFillCameraFill/>,
  star : <AiOutlineStar/>,
  doubleArrLeft: <AiOutlineDoubleLeft/>,
  doubleArrRight: <AiOutlineDoubleRight/>,
  arrBack: <IoIosArrowBack/>,
  arrFor: <IoIosArrowForward/>,
  plus: <AiOutlinePlus/>,
  minus :<AiOutlineMinus/>
};
