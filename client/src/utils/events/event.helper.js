//EVENT ON CLICK GO BACK TO TOP
export const handleBackToTop = () => {
  window.scrollTo(0, 0);
};
