import { useEffect } from "react";

const useScreenSizeChange = (size, callback) => {
  useEffect(() => {
    const mediaScreen = window.matchMedia(`(max-width: ${size}px)`);

    mediaScreen.addEventListener("change", callback);
    callback(mediaScreen);

    return () => {
      mediaScreen.removeEventListener("change", callback);
    };
  }, [size, callback]);
};

export default useScreenSizeChange;
