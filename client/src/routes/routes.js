import { lazy } from "react";
import { MainLayout, SubLayout } from "../layout";
const Home = lazy(() => import("../pages/Home/Home"));
const Login = lazy(() => import("../pages/Login/Login"));
const Register = lazy(() => import("../pages/Register/Register"));
const About = lazy(() => import("../pages/About/About"));
const Contact = lazy(() => import("../pages/Contact/Contact"));
const Cart = lazy(() => import("../pages/Cart/Cart"));
const ProductList = lazy(() =>
  import("../pages/Products/ProductList/ProductList")
);
const ProductDetail = lazy(() =>
  import("../pages/Products/ProductDetail/ProductDetail")
);
const OrderInfo = lazy(()=>import("../pages/OrderInfo/OrderInfo"))

export const publicRoutes = [
  {
    path: "/",
    component: Home,
    layout: MainLayout,
  },
  {
    path: "/login",
    component: Login,
    layout: SubLayout,
  },
  {
    path: "/register",
    component: Register,
    layout: SubLayout,
  },
  {
    path: "/about",
    component: About,
    layout: MainLayout,
  },
  {
    path: "/contact",
    component: Contact,
    layout: MainLayout,
  },
  
  {
    path: "/products",
    component: ProductList,
    layout: MainLayout,
  },

  {
    path: "/products/:productId",
    component: ProductDetail,
    layout: MainLayout,
  },
];

export const privateRoutes = [
  {
    path: "/cart",
    component: Cart,
    layout: MainLayout,
  },
  {
    path: "/order",
    component: OrderInfo,
    layout: MainLayout
  }
];
