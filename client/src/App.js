import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { publicRoutes, privateRoutes } from "./routes/routes";
import { MainLayout } from "./layout";
import { Suspense, Fragment, useState } from "react";
import { useSelector } from "react-redux";
import { Spinner } from "./components/Spinner";
function App() {
  const user = useSelector((state) => state.user.user);
  return (
    <Router>
      <Suspense fallback={<Spinner />}>
        {
          <Routes>
            {publicRoutes.map((route, index) => {
              const Page = route.component;
              let Layout = MainLayout;
              if (route.layout) {
                Layout = route.layout;
              } else if (route.layout === null) {
                Layout = Fragment;
              }
              return (
                <Route
                  key={index}
                  path={route.path}
                  element={
                    <Layout>
                      <Page />
                    </Layout>
                  }
                />
              );
            })}

            {user !== null ? (
              privateRoutes.map((route, index) => {
                const Page = route.component;
                let Layout = MainLayout;
                if (route.layout) {
                  Layout = route.layout;
                } else if (route.layout === null) {
                  Layout = Fragment;
                }

                return (
                  <Route
                    key={index}
                    path={route.path}
                    element={
                      <Layout>
                        <Page />
                      </Layout>
                    }
                  />
                );
              })
            ) : (
              <Route path="*" element={<Navigate to="/login" replace />} />
            )}
          </Routes>
        }
      </Suspense>
    </Router>
  );
}

export default App;
