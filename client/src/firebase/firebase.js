// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, signInWithPopup   } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAHGbfg_u3FsMR4jrjI-Rp37SO3ZpvlQIQ",
  authDomain: "huongsen-5ea75.firebaseapp.com",
  projectId: "huongsen-5ea75",
  storageBucket: "huongsen-5ea75.appspot.com",
  messagingSenderId: "1019583109365",
  appId: "1:1019583109365:web:10fe1ec294d392e3f84dbc"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);

export const provider = new GoogleAuthProvider();

export {signInWithPopup}