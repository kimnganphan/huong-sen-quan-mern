import "./LargeModal.scss";

const LargeModal = ({ isVisibleLarge = false, onClose, title, content, footer }) => {
  return !isVisibleLarge ? null : (
    <div className="lg-modal" onClick={onClose}>
      <div className={`lg-modal-dialog`} onClick={(e) => e.stopPropagation()}>
        <div className="lg-modal-header">
          <h3>{title}</h3>
          <span onClick={onClose}>&times;</span>
        </div>

        <div className="lg-modal-body">
          <div>{content}</div>
        </div>

        {footer && <div className="lg-modal-footer">{footer}</div>}
      </div>
    </div>
  );
};

export default LargeModal;
