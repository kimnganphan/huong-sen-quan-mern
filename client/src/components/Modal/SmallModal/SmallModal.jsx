import { useEffect } from "react";
import "./SmallModal.scss";

const SmallModal = (props) => {
  const { isVisible = false, title, content, footer, onClose } = props;

  return !isVisible ? null : (
    <div className="sm-modal" onClick={onClose}>
      <div className={`sm-modal-dialog`} onClick={(e) => e.stopPropagation()}>
        <div className="sm-modal-header">
          <h3>{title}</h3>
          <span onClick={onClose}>&times;</span>
        </div>

        <div className="sm-modal-body">
          <div>{content}</div>
        </div>

        {footer && <div className="sm-modal-footer">{footer}</div>}
      </div>
    </div>
  );
};

export default SmallModal;
