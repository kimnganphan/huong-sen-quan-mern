import { icons } from "../../../assets/icons";
import "./ChangePageImg.scss";
import { Link } from "react-router-dom";
import { handleBackToTop } from "../../../utils/events/event.helper";
const ChangePageImg = ({ imgSrc, pageName, pagePath }) => {
  return (
    <div className="changePageImg">
      <img src={imgSrc} alt={pageName} />
      <p>{pageName}</p>
      <Link to={pagePath} onClick={handleBackToTop}>
        {icons.arrowRight}
      </Link>
    </div>
  );
};

export default ChangePageImg;
