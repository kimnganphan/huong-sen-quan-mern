import { useEffect } from "react";
import "./Spinner.scss";

const Spinner = () => {
 
  return (
    <div className="spinner">
      <div className="spinner-loader"></div>
      <p>Loading Page....</p>
    </div>
  );
};

export default Spinner;
