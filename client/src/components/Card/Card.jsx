import "./Card.scss";

const Card = ({top, body, footer}) => {
  return (
    <div className="card">
      <div className="card-top">{top}</div>

      <div className="card-body">{body}</div>
      <div className="card-bottom">{footer}</div>
    </div>
  );
};

export default Card;
