import { updateUserByUserId } from "../../api";
import { addProductToCart } from "../../api/carts/cartApi";
import { handleBackToTop } from "../../utils/events/event.helper";

export const onViewMoreProduct = (navigate) => {
  navigate("/products");
  handleBackToTop();
};

export const onViewProductDetail = (productId, navigate) => {
  handleBackToTop();
  navigate(`/products/${productId}`);
};

export const onEditUserInfo = ({
  user,
  userObj,
  dispatch,
  setIsVisibleLarge,
}) => {
  updateUserByUserId(user.user._id, userObj, dispatch, setIsVisibleLarge);
};

export const onAddProductToCartClick = ({ cartId, product, quantity, dispatch, setQuantity }) => {
  const cartProducts = [
    {
      product: product._id,
      quantity,
    },
  ];
  setQuantity(1);
  addProductToCart(cartId, cartProducts, dispatch);
};
