import { Fragment } from "react";

import "./Button.scss";

const Button = ({ buttonTitle, buttonType, handleButtonClick }) => {
  return (
    <Fragment>
      <button className={buttonType} onClick={handleButtonClick}>
        {buttonTitle}
      </button>
    </Fragment>
  );
};

export default Button;
