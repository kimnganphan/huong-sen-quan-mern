import { icons } from "../../assets/icons";
import "./Pagination.scss";

const Pagination = ({ productArr, limit, page, setPage }) => {
  const handlePageClick = (e, type, index) => {
    switch (type) {
      case "first-page":
        setPage(1);
        break;
      case "pre-page":
        setPage(page - 1);
        break;
      case "every-page":
        setPage(index);

        break;
      case "next-page":
        setPage(page + 1);
        break;
      case "last-page":
        setPage(Math.ceil(productArr.length / 6));
        break;
      default:
        break;
    }
  };
  return (
    <div className="pagination">
      {/* ----- FIRST PAGE ---- */}
      <div
        className="pagination-item"
        onClick={(e) => handlePageClick(e, "first-page")}
      >
        <span>{icons.doubleArrLeft}</span>
      </div>
      {/* -----END OF FIRST PAGE ---- */}

      {/* ----- PREVIOUS PAGE ---- */}
      <div
        className="pagination-item"
        onClick={(e) => handlePageClick(e, "pre-page")}
      >
        <span>{icons.arrBack}</span>
      </div>
      {/* -----END OF PREVIOUS PAGE ---- */}

      {/* -----  PAGES ---- */}
      <div className="pagination-item">
        {[...Array(productArr.length)].map((product, index) => {
          if (index >= 1 && index <= Math.ceil(productArr.length / 6)) {
            return (
              <span
                key={index}
                className={page === index ? "activePage" : ""}
                onClick={(e) => handlePageClick(e, "every-page", index)}
              >
                {index}
              </span>
            );
          }
        })}
      </div>
      {/* -----END OF  PAGES ---- */}

      {/* ----- NEXT PAGE ---- */}
      <div
        className="pagination-item"
        onClick={(e) => handlePageClick(e, "next-page")}
      >
        <span>{icons.arrFor}</span>
      </div>
      {/* -----END OF NEXT PAGE ---- */}

      {/* ----- LAST PAGE ---- */}
      <div
        className="pagination-item"
        onClick={(e) => handlePageClick(e, "last-page")}
      >
        <span>{icons.doubleArrRight}</span>
      </div>
      {/* -----END OF LAST PAGE ---- */}
    </div>
  );
};

export default Pagination;
