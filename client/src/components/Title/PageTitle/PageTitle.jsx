
import './PageTitle.scss'

const PageTitle = ({content}) => {
  return (
    <div className='page-title'>
        <h2>{content}</h2>
        <img src="https://cdn.shopify.com/s/files/1/0575/7911/0557/files/section-heading_decoration.svg?v=1626150778" alt="heading-img" />
    </div>
  )
}

export default PageTitle