import "./SectionTitle.scss";

const SectionTitle = ({ title, subTitle, icon }) => {
  return (
    <div className="section-title">
      <p className="section-title-icon">{icon}</p>
      <h4>{title}</h4>
      <p>{subTitle}</p>
    </div>
  );
};

export default SectionTitle;
