import "./Products.scss";
import { useSelector } from "react-redux";
import { Card } from "../Card";
import { Button, onViewProductDetail } from "../Button";
import { Fragment } from "react";
import { numberWithCommas } from "../../utils/currency/currency.helper";
import { useNavigate } from "react-router-dom";

const Products = () => {
  const product = useSelector((state) => state.product);
  const navigate = useNavigate();
  return (
    <div className="products">
      {product.paginationProducts.map((product) => {
        return (
          <Card
            top={
              <div className="card-imageWrapper">
                <div className="card-image">
                  <img src={product.img} alt={product.title} />
                </div>
                <div className="card-icon"></div>
              </div>
            }
            body={
              <Fragment>
                <h3>{product.title?.toUpperCase()}</h3>
                <p>{product.price && numberWithCommas(product.price)} VNĐ</p>
              </Fragment>
            }
            footer={
              <Button
                buttonTitle={`Chi Tiết`}
                buttonType="light"
                handleButtonClick={() =>
                  onViewProductDetail(product._id, navigate)
                }
              />
            }
            key={product._id}
          />
        );
      })}
    </div>
  );
};

export default Products;
