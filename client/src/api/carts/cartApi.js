import { publicRequest } from "../baseApi";
import { deleteItemFromCart, getCartProducts } from "../../store/carts/cart.slice";

export const createCart = async (userId, dispatch) => {
    try {
        const res = await publicRequest.post(`carts`, {user:userId})
        dispatch(getCartProducts({id: res.data.cart._id, products: res.data.cart.cartProducts}))
    } catch (error) {
        
    }
}

export const addProductToCart = async (user, cartProducts, dispatch) => {
    try {
        const res = await publicRequest.post("carts", {user, cartProducts})
       createCart(user, dispatch)
    } catch (error) {
        
    }
}


export const deleteProductFromCart = async (user, productId, dispatch) => {
    try {
        const res = await publicRequest.post("carts/delete-item", {user, productId})
        dispatch(deleteItemFromCart({id: productId}))
    } catch (error) {
        
    }
}


export const updateProductInCart = async (user, type, productId, dispatch) => {
    try {
        const res = await publicRequest.post("carts/update-cart", {user, type, productId})
        dispatch(getCartProducts({id: res.data.cart._id, products: res.data.cart.cartProducts}))
    } catch (error) {
        
    }
}

export const emptyCartAfterCreateOrder = async (user) => {
    try {
        await publicRequest.post("carts/empty-cart", {user})
    } catch (error) {
        
    }
}