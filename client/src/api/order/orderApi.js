import { clearCartAfterSubmit } from "../../store/carts/cart.slice";
import { getAllOrders } from "../../store/orders/order.slice";
import { publicRequest } from "../baseApi";
import { emptyCartAfterCreateOrder } from "../carts/cartApi";

export const createOrder = async (
  order,
  dispatch,
  setIsVisibleLarge,
  setContent,
  userId
) => {
  try {
    const res = await publicRequest.post("orders/create-order", order);
    setIsVisibleLarge(true);
    setContent(`Mã Đơn Hàng: ${res.data.order._id}`);
    dispatch(clearCartAfterSubmit())
    emptyCartAfterCreateOrder(userId)
  } catch (error) {}
};


export const getOrderByUserId = async (user, dispatch) => {
    try {
        const res = await publicRequest.post("orders/get-user-order", {user})
        dispatch(getAllOrders(res.data.order))
    } catch (error) {
        
    }
}