import { publicRequest } from "../baseApi";
import { loginSuccess, updateUser } from "../../store/users/user.slice";
import { createCart } from "../carts/cartApi";
import { getUserId } from "../../store/carts/cart.slice";

//REGISTER NEW USER
export const registerUser = async (
  input,
  setInput,
  setIsVisible,
  setTitle,
  setContent
) => {
  try {
    await publicRequest.post("users", input);
    //RESET INPUT
    setInput({});
    // OPEN MODAL TO ANNOUNCE REGISTER SUCCESSFULLY
    setIsVisible(true);
    setTitle("Thông Báo");
    setContent("Đã Tạo Tài Khoản Thành Công");
  } catch (error) {}
};

//LOGIN USER

export const loginUser = async (input, dispatch, navigate, setIsVisible, setContent) => {
  try {
    const res = await publicRequest.post("users/login", input);
    

    if (res.data.user !== null) {
      navigate("/");
      createCart(res.data.user._id, dispatch)
      dispatch(loginSuccess(res.data.user));
      dispatch(getUserId(res.data.user._id))
    } else {
      setIsVisible(true);
      setContent('Username hoặc password bị sai!');
    }
  } catch (error) {}
};

export const updateUserByUserId = async (userId, user, dispatch, setIsVisibleLarge) => {
  try {
    const res = await publicRequest.put(`users/${userId}`, user);
    dispatch(updateUser(res.data.updatedUser));
    setIsVisibleLarge(false);
  } catch (error) {}
};
