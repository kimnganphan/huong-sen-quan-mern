import { publicRequest } from "../baseApi";
import {
  getProducts,
  getPaginationProducts,
  getSingleProduct,
  relatedProduct
} from "../../store/products/product.slice";
//FUNCTION GET ALL PRODUCTS FROM API
export const getAllProductsFromApi = async (
  cat,
  title,
  limit,
  page,
  dispatch
) => {
  try {
    const res = await publicRequest.get(
      `products?${cat
        ?.map((cat) => {
          return `cat=${cat}`;
        })
        .join("&")}&title=${title}&limit=${limit}&page=${page}`
    );
    if (limit === 0) {
      dispatch(getProducts(res.data.products)); //PHÂN TRANG (TRẢ VỀ TẤT CẢ CÁC SẢN PHẨM)
      // K FILTER GÌ HẾT => 30 SẢN PHẨM
      //FILTER SẢN PHẨM (SP ĐÓ CÓ 12 SP) => 12 SẢN PHẨM
     
    } else  {
      dispatch(getPaginationProducts(res.data.products)); //HIỂN THỊ SẢN PHẨM LIMIT LÊN TRÌNH DUYỆT (6 SẢN PHẨM)
      // K FILTER GÌ HẾT => 6 SẢN PHẨM TRÊN TỔNG 30 SẢN PHẨM
      //FILTER SẢN PHẨM (SP ĐÓ CÓ 12 SP) => 6 SẢN PHẨM

     
    }
  } catch (error) {}
};

//GET SINGLE PRODUCT BY PRODUCT ID
export const getSingleProductByProductId = async (productId, dispatch) => {
  try {
    const res = await publicRequest.get(`products/${productId}`);
    dispatch(getSingleProduct(res.data.product));
    dispatch(relatedProduct({id: res.data.product._id, categories: res.data.product.categories}))
  } catch (error) {}
};
