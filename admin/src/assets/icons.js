import {
  AiOutlineUser,
  AiOutlineLock,
  AiOutlineEye,
  AiOutlineCheckCircle,
  AiOutlineEyeInvisible,
  AiOutlineDoubleLeft,
  AiOutlineDoubleRight,
  AiOutlineClose,
  AiOutlineCheck,
  AiOutlineExclamation
} from "react-icons/ai";
import { MdDashboard, MdStorefront } from "react-icons/md";
import { RiBillFill } from "react-icons/ri";
import { BsGlobe, BsInfo } from "react-icons/bs";
import {IoIosArrowBack,IoIosArrowForward} from 'react-icons/io'
export const icons = {
  userIcon: <AiOutlineUser />,
  lockIcon: <AiOutlineLock />,
  eyeIcon: <AiOutlineEye />,
  inviEye: <AiOutlineEyeInvisible />,
  checkIcon: <AiOutlineCheckCircle />,
  dashIcon: <MdDashboard />,
  storeIcon: <MdStorefront />,
  billIcon: <RiBillFill />,
  viewIcon: <BsGlobe />,
  doubleArrLeft: <AiOutlineDoubleLeft />,
  doubleArrRight: <AiOutlineDoubleRight />,
  arrLeft: <IoIosArrowBack />,
  arrRight: <IoIosArrowForward />,
  close: <AiOutlineClose/>,
  check: <AiOutlineCheck/>,
  exclamation: <AiOutlineExclamation/>,
  info: <BsInfo/>
};
