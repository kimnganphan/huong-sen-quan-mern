import { MainLayout } from "../layout/MainLayout";
import { lazy } from "react";
const Home = lazy(() => import("../pages/Home/Home"));

const NewProduct = lazy(() =>
  import("../pages/Products/NewProduct/NewProduct")
);
const ProductDetail = lazy(() =>
  import("../pages/Products/ProductDetail/ProductDetail")
);
const Products = lazy(() => import("../pages/Products/Products"));

const NewUser = lazy(() => import("../pages/Users/NewUser/NewUser"));
const Users = lazy(() => import("../pages/Users/Users"));

const NewOrder = lazy(() => import("../pages/Orders/NewOrder/NewOrder"));
const Orders = lazy(() => import("../pages/Orders/Orders"));
const UserDetail = lazy(()=> import('../pages/Users/UserDetail/UserDetail'))
const publicRoutes = [
  {
    path: "/",
    component: Home,
    layout: MainLayout,
  },
  {
    path: "/products",
    component: Products,
    layout: MainLayout,
  },
  {
    path: "/products/new",
    component: NewProduct,
    layout: MainLayout,
  },
  {
    path: "/products/:productId",
    component: ProductDetail,
    layout: MainLayout,
  },
  {
    path: "/users",
    component: Users,
    layout: MainLayout,
  },
  {
    path: "/users/new",
    component: NewUser,
    layout: MainLayout,
  },
  {
    path: "/users/:userId",
    component: UserDetail,
    layout: MainLayout,
  },
  {
    path: "/orders",
    component: Orders,
    layout: MainLayout,
  },
  {
    path: "/orders/new",
    component: NewOrder,
    layout: MainLayout,
  },
];

export default publicRoutes;
