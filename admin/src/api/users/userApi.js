import {
  deleteUser,
  getAllUsers,
  getPaginationUsers,
  getSingleUser,
  updateUser,
} from "../../store/users/users.slice";
import { publicRequest } from "../baseApi";

//HANDLE GET ALL USERS
export const getAllUsersFromApi = async ({ limit, page, dispatch }) => {
  try {
    if (limit) {
      const res = await publicRequest.get(`users?limit=${limit}&page=${page}`);

      dispatch(getPaginationUsers(res.data.users));
    } else {
      const res = await publicRequest.get(`users`);
      dispatch(getAllUsers(res.data.users));
    }
  } catch (error) {}
};

//HANDLE DELETE USER BY USER ID
export const deleteUserByUserId = async (userId, dispatch, onClose) => {
  try {
    const res = await publicRequest.delete(`users/${userId}`);
    dispatch(deleteUser(userId));
    onClose();
  } catch (error) {}
};

//HANDLE GET USER BY USER ID
export const getSingleUserByUserId = async ({ userId, dispatch }) => {
  try {
    const res = await publicRequest.get(`users/${userId}`);
    dispatch(getSingleUser(res.data.user));
  } catch (error) {}
};

//HANDLE UPDATE USER BY ID
export const updateUserByUserId = async ({
  userId,
  user,
  dispatch,
  onClose,
  setIsToastVisible,
  setToastType,
  setToastMessage,
}) => {
  try {
    const res = await publicRequest.put(`users/${userId}`, user);
    dispatch(updateUser(res.data.updatedUser));
    onClose();
    setIsToastVisible(true)
    setToastType('success')
    setToastMessage('Đã Cập Nhật Thông Tin Thành Công')
  } catch (error) {
    setIsToastVisible(true)
    setToastType('danger')
    setToastMessage(error)
  }
};
