import { publicRequest } from "../baseApi";
import {
  addProduct,
  updateProduct,
  getProducts,
  getPaginationProducts,
  deleteProduct,
  getSingleProduct,
  setDesc,
  setTitle,
  setPrice,
} from "../../store/products/product.slice";
//FUNCTION GET ALL PRODUCTS FROM API
export const getAllProductsFromApi = async (limit, page, dispatch) => {
  try {
    const res = await publicRequest.get(
      `products?limit=${limit}&page=${page}`
    );
    if (limit === 0) {
      dispatch(getProducts(res.data.products));
    } else {
      dispatch(getPaginationProducts(res.data.products));
    }
  } catch (error) {}
};

//CREATE NEW PRODUCT
export const createNewProduct = async ({
  product,
  dispatch,
  resetDataAfterSubmitSuccess,
  setModalTitle,
  setContent,
  setIsVisible,
  setIsToastVisible,
  setToastType,
  setToastMessage,
}) => {
  try {
    const res = await publicRequest.post("products", product);
    dispatch(addProduct(res.data.product));
    //MODAL
    setModalTitle("Thông Báo");
    setContent("Đã Tạo Thành Công Sản Phẩm");
    setIsVisible(true);

    //RESET DATA
    resetDataAfterSubmitSuccess();

    //TOAST
    setIsToastVisible(true);
    setToastType("success");
    setToastMessage("Đã Tạo Sản Phẩm Mới Thành Công");
  } catch (error) {
    //TOAST
    setIsToastVisible(true);
    setToastType("danger");
    setToastMessage(error);
  }
};

//DELETE PRODUCT BY PRODUCT ID
export const deleteProductByProductId = async ({
  productId,
  dispatch,
  onClose,
  setIsToastVisible,
  setToastType,
  setToastMessage,
}) => {
  try {
    await publicRequest.delete(`products/${productId}`);

    dispatch(deleteProduct(productId));
    //MODAL
    onClose();
    //TOAST
    setIsToastVisible(true);
    setToastType("success");
    setToastMessage("Xóa Sản Phẩm Thành Công");
  } catch (error) {
    setIsToastVisible(true);
    setToastType("danger");
    setToastMessage(error);
  }
};

//GET SINGLE PRODUCT BY PRODUCT ID
export const getSingleProductByProductId = async (productId, dispatch) => {
  try {
    const res = await publicRequest.get(`products/${productId}`);
    dispatch(getSingleProduct(res.data.product));
  } catch (error) {}
};

//UPDATE PRODUCT BY PRODUCT ID

export const updateProductByProductId = async ({
  productId,
  product,
  dispatch,
  onClose,
  setIsToastVisible,
  setToastType,
  setToastMessage,
}) => {
  try {
    const res = await publicRequest.put(`products/${productId}`, product);

    dispatch(updateProduct({ id: productId, product }));
    //RESET PRODUCT DATA
    dispatch(setTitle(""));
    dispatch(setDesc(""));
    dispatch(setPrice(""));
    //CLOSE MODAL AFTER SUCCESS
    onClose();
    //SET TOAST
    setIsToastVisible(true);
    setToastType("success");
    setToastMessage("Đã Cập Nhật Sản Phẩm Thành Công");
  } catch (error) {
    setIsToastVisible(true);
    setToastType("danger");
    setToastMessage(error);
  }
};
