import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  theme: "default"
};

export const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    chooseDarkTheme: (state) => {
      state.theme = "dark"
    },
    choosePinkTheme: (state) => {
      state.theme = "pink"
    },
    chooseBlueTheme: (state) => {
      state.theme = "blue"
    },
    chooseDefault: (state) => {
      state.theme = "default"
    },
  },
});

export const {
  chooseDarkTheme,
  choosePinkTheme,
  chooseBlueTheme,
  chooseDefault,
} = themeSlice.actions;

export default themeSlice.reducer;
