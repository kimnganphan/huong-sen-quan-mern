import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: {},
  users: [],
  pagiUsers: [],
  firstName: "",
  lastName: "",
  username: "",
  email: "",
  address: "",
  phoneNumber: "",
  avatar: ""
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    getAllUsers: (state, action) => {
      state.users = action.payload;
    },
    getPaginationUsers: (state, action) => {
      state.pagiUsers = action.payload;
    },
    getSingleUser: (state, action) => {
      state.user = action.payload;
    },
    deleteUser: (state, action) => {
      state.users.splice(
        state.users.findIndex((item) => item._id === action.payload),
        1
      );
    },
    updateUser: (state, action) => {
      state.user = action.payload;
    },
    setFirstName: (state, action) => {
      state.firstName = action.payload;
    },
    setLastName: (state, action) => {
      state.lastName = action.payload;
    },
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    setUsername: (state, action) => {
      state.username = action.payload;
    },
    setAddress: (state, action) => {
      state.address = action.payload;
    },
    setPhoneNumber: (state, action) => {
      state.phoneNumber = action.payload;
    },
    setAvatar: (state, action) => {
      state.avatar = action.payload;
    }
  },
});

export const {
  getAllUsers,
  getPaginationUsers,
  getSingleUser,
  deleteUser,
  updateUser,
  setFirstName,
  setLastName,
  setEmail,
  setUsername,
  setAddress,
  setPhoneNumber,
  setAvatar
} = userSlice.actions;

export default userSlice.reducer;
