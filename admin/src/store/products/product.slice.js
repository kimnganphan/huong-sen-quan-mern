import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  title: "",
  desc: "",
  price: "",
  img: "",
  galleryImg: [],
  categories: [],
  product: {},
  products: [],
  paginationProducts: [],
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.products.push(action.payload);
    },
    updateProduct: (state, action) => {
      state.products[
        state.products.findIndex((item) => item._id === action.payload.id)
      ] = action.payload.product;
    },
    getProducts: (state, action) => {
      state.products = action.payload;
    },
    getPaginationProducts: (state, action) => {
      state.paginationProducts = action.payload;
    },
    deleteProduct: (state, action) => {
      state.products.splice(
        state.products.findIndex((item) => item._id === action.payload),
        1
      );
    },
    getSingleProduct: (state, action) => {
      state.product = action.payload;
    },
    setTitle: (state, action) => {
      state.title = action.payload;
    },

    setDesc: (state, action) => {
      state.desc = action.payload;
    },

    setPrice: (state, action) => {
      state.price = action.payload;
    },
    setImg: (state, action) => {
      state.img = action.payload;
    },
    setGalleryImg: (state, action) => {
      state.galleryImg = action.payload;
    },
    setCategories: (state, action) => {
      state.categories = action.payload;
    }
   
  },
});

export const {
  addProduct,
  updateProduct,
  getProducts,
  getPaginationProducts,
  deleteProduct,
  getSingleProduct,
  setTitle,
  setDesc,
  setPrice,
  setImg,
  setGalleryImg,
  setCategories
} = productSlice.actions;

export default productSlice.reducer;
