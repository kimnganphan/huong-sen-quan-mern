import { Button } from "../../components/Button";
const handleDeleteButtonClick = ({ setIsVisible, setUserId, id }) => {
  setIsVisible(true);
  setUserId(id);
};
export const userConstant = ({ Link }) => {
  return [
    {
      title: "STT",
      render: ({ rowData, index, page }) => {
        return <span>{index + 1 + (page - 1) * 6}</span>;
      },
    },
    {
      title: "Username",
      render: ({ rowData }) => {
        return (
          <>
            <span>{rowData.username}</span>
          </>
        );
      },
    },
    {
      title: "Hoạt Động",
      render: ({ rowData, setIsVisible, setUserId }) => {
        return (
          <>
            <Link to={`/users/${rowData._id}`}>
              <Button title={`Sửa`} type="red" />
            </Link>

            <Button
              title="Xóa"
              type="green"
              handleButtonClick={() =>
                handleDeleteButtonClick({
                  setIsVisible,
                  setUserId,
                  id: rowData._id,
                })
              }
            />
          </>
        );
      },
    },
  ];
};
