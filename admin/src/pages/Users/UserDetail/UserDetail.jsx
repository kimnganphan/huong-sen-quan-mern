import React, { useEffect, useState } from "react";
import "./UserDetail.scss";
import { useDispatch, useSelector } from "react-redux";
import { Button, onEditUserClick } from "../../../components/Button";
import { LargeModal } from "../../../components/Modal";
import { useParams } from "react-router-dom";
import { getSingleUserByUserId } from "../../../api/users/userApi";
import { images } from "../../../assets/images";
import InputDetail from "./components/InputDetail/InputDetail";
import { Toast } from "../../../components/Toast";
const UserDetail = () => {
  //REDUX
  const theme = useSelector((state) => state.theme.theme);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  //MODAL
  const [isVisible, setIsVisbile] = useState(false);
  //BUTTON
  const [disabled, setDisabled] = useState(false);
  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");
  //ROUTER
  const id = useParams();

  const userObj = {
    firstName: user.firstName,
    lastName: user.lastName,
    username: user.username,
    email: user.email,
    address: user.address,
    phoneNumber: user.phoneNumber,
    avatar: user.avatar,
  };
  //GET USER BY USER ID
  useEffect(() => {
    getSingleUserByUserId({ userId: id.userId, dispatch });
  }, [id.userId, dispatch]);

  //VALIDATE USER INFO BEFORE EDIT
  const validateUserInfoBeforeSubmit = () => {
    //VALIDATE LASTNAME
    if (userObj.lastName.trim() === "") {
      setToastMessage("Vui Lòng Nhập Họ");
      return false;
    }

    //VALIDATE FIRSTNAME
    if (userObj.firstName.trim() === "") {
      setToastMessage("Vui Lòng Nhập Tên");
      return false;
    }

    //VALIDATE USERNAME
    const userFound = user.users.find((user) => {
      return user._id !== id.userId && user.username === userObj.username;
    });
    if (userObj.username.trim() === "") {
      setToastMessage("Vui Lòng Nhập Username");
      return false;
    }
    if (userFound) {
      setToastMessage("Username đã tồn tại!");
      return false;
    }

    //VALIDATE EMAIL
    const emailFound = user.users.find((user) => {
      return user._id !== id.userId && user.email === userObj.email;
    });
    if (userObj.email.trim() === "") {
      setToastMessage("Vui lòng nhập email");
      return false;
    }

    if (emailFound) {
      setToastMessage("Email đã tồn tại!");
      return false;
    }
    return true;
  };
  return (
    <div className="userDetail" data-theme={theme}>
      <h3 data-page-title>CHI TIẾT KHÁCH HÀNG</h3>

      <div className="userDetail-content">
        {/* ----USER THUMBNAIL IMAGE------ */}
        <div className="userDetail-content-item">
          <div className="userDetail-content-item-left">
            <label>Ảnh Đại Diện Khách Hàng:</label>
          </div>
          <div className="userDetail-content-item-right">
            {user.user.avatar ? (
              <img src={user.user.avatar} alt="" />
            ) : (
              <img src={images.noImage} alt="" />
            )}
          </div>
        </div>

        {/* ----END OF USER THUMBNAIL IMAGE------ */}

        {/* ----USER NAME------ */}
        <div className="userDetail-content-item">
          <div className="userDetail-content-item-left">
            <label>Tên Khách Hàng:</label>
          </div>
          <div className="userDetail-content-item-right">
            <h4 data-product-title>
              {user.user.lastName} {user.user.firstName}
            </h4>
          </div>
        </div>

        {/* ----END OF USER NAME------ */}

        {/* ----USERNAME------ */}
        <div className="userDetail-content-item">
          <div className="userDetail-content-item-left">
            <label>Username:</label>
          </div>
          <div className="userDetail-content-item-right">
            {user.user.username}
          </div>
        </div>

        {/* ----END OF USERNAME----- */}

        {/* ----EMAIL------ */}
        <div className="userDetail-content-item">
          <div className="userDetail-content-item-left">
            <label>Email:</label>
          </div>
          <div className="userDetail-content-item-right">{user.user.email}</div>
        </div>

        {/* ----END OF EMAIL------ */}

        {/* ----ADDRESS------ */}
        <div className="userDetail-content-item">
          <div className="userDetail-content-item-left">
            <label>Địa Chỉ:</label>
          </div>
          <div className="userDetail-content-item-right">
            {user.user?.address}
          </div>
        </div>

        {/* ----END OF ADDRESS------ */}

        {/* ----USER PHONE NUMBER------ */}
        <div className="userDetail-content-item">
          <div className="userDetail-content-item-left">
            <label>Số Điện Thoại:</label>
          </div>
          <div className="userDetail-content-item-right">
            {user.user?.phoneNumber}
          </div>
        </div>

        {/* ----END OF USER PHONE NUMBER------ */}

        <div className="userDetail-content-button">
          <Button
            title="Sửa Thông Tin Khách Hàng"
            handleButtonClick={() => setIsVisbile(true)}
            type="light"
          />
        </div>
      </div>

      <LargeModal
        isVisible={isVisible}
        onClose={() => setIsVisbile(false)}
        title="Chi Tiết Thông Tin Khách Hàng"
        content={<InputDetail setDisabled={setDisabled} />}
        footer={
          <>
            <Button
              title="Đóng"
              handleButtonClick={() => setIsVisbile(false)}
              type="red"
            />
            <Button
              title="Xác Nhận"
              type={`green`}
              disabled={disabled}
              handleButtonClick={() =>
                onEditUserClick({
                  userId: id.userId,
                  onClose: () => setIsVisbile(false),
                  dispatch,
                  user: userObj,
                  validateUserInfoBeforeSubmit,
                  setIsToastVisible,
                  setToastType,
                  setToastMessage,
                })
              }
            />
          </>
        }
      />

      <Toast
        isToastVisible={isToastVisible}
        onCloseToast={() => setIsToastVisible(false)}
        type={toastType}
        message={toastMessage}
      />
    </div>
  );
};

export default UserDetail;
