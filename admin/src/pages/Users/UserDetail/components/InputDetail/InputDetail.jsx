import "./InputDetail.scss";
import { useDispatch, useSelector } from "react-redux";
import {
  setAddress,
  setAvatar,
  setEmail,
  setFirstName,
  setLastName,
  setPhoneNumber,
  setUsername,
} from "../../../../../store/users/users.slice";
import { useEffect, useState } from "react";
import { images } from "../../../../../assets/images";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { app } from "../../../../../firebase/firebase";
import { Toast } from "../../../../../components/Toast";
const InputDetail = ({setDisabled}) => {
  //REDUX
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  //STATE
  const [fileImg, setFileImg] = useState(null);
  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");
 
  useEffect(() => {
    dispatch(setFirstName(user.user.firstName));
    dispatch(setLastName(user.user.lastName));
    dispatch(setUsername(user.user.username));
    dispatch(setEmail(user.user.email));
    dispatch(setAddress(user.user.address ? user.user.address : ""));
    dispatch(
      setPhoneNumber(user.user.phoneNumber ? user.user.phoneNumber : "")
    );
    dispatch(setAvatar(user.user.avatar ? user.user.avatar : images.noImage));

  }, [dispatch, user.user]);

  const handleEditUserImage = (e) => {
    setFileImg(e.target.files[0]);
  };

  useEffect(() => {
    if (fileImg != null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, fileImg.name);

      const uploadTask = uploadBytesResumable(storageRef, fileImg);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          //DISABLE BUTTON WHILE UPLOAD
          setDisabled(true);
          //INFORM TOAST
          setIsToastVisible(true);
          setToastType("info");
          setToastMessage("Ảnh Đang Được Upload. Vui Lòng Chờ...");
        },
        (error) => {
          // Handle unsuccessful uploads

          //TOAST
          setIsToastVisible(true);
          setToastType("danger");
          setToastMessage(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            //RESET BUTTON AFTER UPLOAD SUCCESSFUL
            setDisabled(false);
            //SUCCESS INFORM TOAST
            setIsToastVisible(true);
            setToastType("success");
            setToastMessage("Đã Upload Ảnh Thành Công");
            //RESET IMAGE
            setFileImg(null);
            dispatch(setAvatar(downloadURL));
          });
        }
      );
    }
  }, [fileImg]);
  return (
    <div className="user-inputDetail">
      {/* --------- USER AVATAR ------- */}
      <div className="user-inputDetail-item">
        <label>Ảnh Đại Diện</label>
        <div className="user-image">
          <div className="user-image-layout">
            <input
              type="file"
              id="file"
              style={{ display: "none" }}
              onChange={handleEditUserImage}
            />
            <label htmlFor="file">Sửa</label>
          </div>
          <img src={user.avatar} alt="" />
        </div>
      </div>
      {/* --------- USER AVATAR ------- */}
      {/* ----- LASTNAME ----- */}
      <div className="user-inputDetail-item">
        <label>Họ</label>
        <input
          type="text"
          value={user.lastName}
          onChange={(e) => dispatch(setLastName(e.target.value))}
        />
      </div>
      {/* ----- END OF LASTNAME ----- */}

      {/* ----- FIRSTNAME ----- */}
      <div className="user-inputDetail-item">
        <label>Tên</label>
        <input
          type="text"
          value={user.firstName}
          onChange={(e) => dispatch(setFirstName(e.target.value))}
        />
      </div>
      {/* ----- END OF FIRSTNAME ----- */}

      {/* ----- USERNAME ----- */}
      <div className="user-inputDetail-item">
        <label>Username</label>
        <input
          type="text"
          value={user.username}
          onChange={(e) => dispatch(setUsername(e.target.value))}
        />
      </div>
      {/* ----- END OF USERNAME ----- */}

      {/* ----- EMAIL ----- */}
      <div className="user-inputDetail-item">
        <label>Email</label>
        <input
          type="text"
          value={user.email}
          onChange={(e) => dispatch(setEmail(e.target.value))}
        />
      </div>
      {/* ----- END OF EMAIL ----- */}

      {/* ----- ADDRESS ----- */}
      <div className="user-inputDetail-item">
        <label>Địa Chỉ</label>
        <input
          type="text"
          value={user.address}
          onChange={(e) => dispatch(setAddress(e.target.value))}
        />
      </div>
      {/* ----- END OF ADDRESS ----- */}

      {/* ----- PHONE NUMBER ----- */}
      <div className="user-inputDetail-item">
        <label>Số Điện Thoại</label>
        <input
          type="text"
          value={user.phoneNumber}
          onChange={(e) => dispatch(setPhoneNumber(Number(e.target.value)))}
        />
      </div>
      {/* ----- END OF PHONE NUMBER ----- */}

      <Toast
        isToastVisible={isToastVisible}
        onCloseToast={() => setIsToastVisible(false)}
        type={toastType}
        message={toastMessage}
      />
    </div>
  );
};

export default InputDetail;
