import "./Users.scss";
import { Button, onGoToAddNewClick } from "../../components/Button";
import { useLocation, useNavigate, Link } from "react-router-dom";
import Table from "../../components/Table/Table";
import { userConstant } from "./Users.constant";
import { useEffect, useState } from "react";
import { getAllUsersFromApi } from "../../api/users/userApi";
import { useDispatch, useSelector } from "react-redux";
import { TablePagination } from "../../components/Pagination";

const Users = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const dispatch = useDispatch();
  const state = useSelector((state) => state.user);
  const limit = 6;
  const [page, setPage] = useState(1);

  //GET LIMIT USERS
  useEffect(() => {
    getAllUsersFromApi({ limit, page, dispatch });
  }, [limit, page, dispatch, state.users]);

  //GET ALL USER
  useEffect(() => {
    getAllUsersFromApi({ dispatch });
  }, [dispatch]);
  return (
    <div className="users">
      <h3 data-page-title>DANH SÁCH KHÁCH HÀNG</h3>
      <div className="users-top">
        <Button
          title="Thêm Mới Khách Hàng"
          type={`dark-green`}
          handleButtonClick={() =>
            onGoToAddNewClick({ location: location.pathname, navigate })
          }
        />
      </div>

      <div className="users-table">
        <Table cols={userConstant({Link})} data={state.pagiUsers} page={page}/>
      </div>

      <div className="users-pagination">
        <TablePagination
          paginationArr={state.users}
          limit={limit}
          page={page}
          setPage={setPage}
        />
      </div>
    </div>
  );
};

export default Users;
