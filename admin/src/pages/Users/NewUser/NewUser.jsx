import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { images } from "../../../assets/images";
import "./NewUser.scss";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { app } from "../../../firebase/firebase";
import { Button } from "../../../components/Button";
import { Toast } from "../../../components/Toast";
const NewUser = () => {
  const [fileImg, setFileImg] = useState(null);
  const [avatarSrc, setAvatarSrc] = useState("");

  const [input, setInput] = useState("");

  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");

  //HANDLE CHANGE AVATAR
  const handleChangeAvatar = (e) => {
    setFileImg(e.target.files[0]);
  };

  useEffect(() => {
    if (fileImg != null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, fileImg.name);

      const uploadTask = uploadBytesResumable(storageRef, fileImg);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          //DISABLE BUTTON WHILE UPLOAD
          // setDisabled(true);
          //INFORM TOAST
          //setIsToastVisible(true);
          //setToastType("info");
          // setToastMessage("Ảnh Đang Được Upload. Vui Lòng Chờ...");
        },
        (error) => {
          // Handle unsuccessful uploads
          //TOAST
          // setIsToastVisible(true);
          // setToastType("danger");
          // setToastMessage(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            //RESET BUTTON AFTER UPLOAD SUCCESSFUL
            // setDisabled(false);
            //SUCCESS INFORM TOAST
            // setIsToastVisible(true);
            //setToastType("success");
            // setToastMessage("Đã Upload Ảnh Thành Công");
            //RESET IMAGE
            setFileImg(null);
            setAvatarSrc(downloadURL);
          });
        }
      );
    }
  }, [fileImg]);
  //HANDLE ONCHANGE INPUT
  const handleInput = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  //HANDLE ADD NEW USER CLICK
  const handleAddNewUser = () => {
   
  };


  //VALIDATE USER INFO BEFORE SUBMIT
  const validateUserInfoBeforeSubmit = () => {
    if(!input.lastName){

      return false;
    }
    return true
  }
  return (
    <div className="newUser">
      {/* ------------- TITLE ---------- */}
      <h3 data-page-title>THÊM MỚI KHÁCH HÀNG</h3>
      {/* ------------- END OF TITLE ---------- */}

      {/* --------- CONTENT ------------- */}
      <div className="newUser-content">
        {/* ------- AVATAR ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <img src={avatarSrc === "" ? images.noImage : avatarSrc} alt="" />
          </div>
          <div className="newUser-content-item-right">
            <input type="file" onChange={handleChangeAvatar} />
          </div>
        </div>
        {/* -------END OF AVATAR ----- */}

        {/* ------------ LAST NAME ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <label>Họ</label>
          </div>
          <div className="newUser-content-item-right">
            <input
              type="text"
              name="lastName"
              placeholder="Vui lòng nhập họ"
              onChange={handleInput}
              value={input.lastName ?? ""}
            />
          </div>
        </div>
        {/* ------------END OF LAST NAME ----- */}

        {/* ------------ FIRST NAME ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <label>Tên</label>
          </div>
          <div className="newUser-content-item-right">
            <input
              type="text"
              name="firstName"
              placeholder="Vui lòng nhập tên"
              onChange={handleInput}
              value={input.firstName ?? ""}
            />
          </div>
        </div>
        {/* ------------END OF FIRST NAME ----- */}

        {/* ------------ USERNAME ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <label>Username</label>
          </div>
          <div className="newUser-content-item-right">
            <input
              type="text"
              name="username"
              placeholder="Vui lòng nhập username"
              onChange={handleInput}
              value={input.username ?? ""}
            />
          </div>
        </div>
        {/* ------------END OF USERNAME ----- */}

        {/* ------------ EMAIL ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <label>Email</label>
          </div>
          <div className="newUser-content-item-right">
            <input
              type="text"
              name="email"
              placeholder="Vui lòng nhập email"
              onChange={handleInput}
              value={input.email ?? ""}
            />
          </div>
        </div>
        {/* ------------END OF EMAIL ----- */}

        {/* ------------ ADDRESS ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <label>Địa Chỉ</label>
          </div>
          <div className="newUser-content-item-right">
            <input
              type="text"
              name="address"
              placeholder="Vui lòng nhập địa chỉ"
              onChange={handleInput}
              value={input.address ?? ""}
            />
          </div>
        </div>
        {/* ------------END OF ADDRESS ----- */}

        {/* ------------ PHONENUMBER ----- */}
        <div className="newUser-content-item">
          <div className="newUser-content-item-left">
            <label>Số Điện Thoại</label>
          </div>
          <div className="newUser-content-item-right">
            <input
              type="text"
              name="phoneNumber"
              placeholder="Vui lòng nhập số điện thoại"
              onChange={handleInput}
              value={input.phoneNumber ?? ""}
            />
          </div>
        </div>
        {/* ------------END OF PHONENUMBER ----- */}

        {/* ---- ADD BUTTON ----- */}
        <div className="newUser-content-item">
          <Button
            title="Thêm Khách Hàng Mới"
            type="light"
            handleButtonClick={handleAddNewUser}
          />
        </div>
        {/* ---- END OF ADD BUTTON ----- */}
      </div>
      {/* --------- END OF CONTENT ------------- */}

      <Toast/>
    </div>
  );
};

export default NewUser;
