import "./Products.scss";
import { Button, onGoToAddNewClick } from "../../components/Button";

import { TablePagination } from "../../components/Pagination";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProductsFromApi } from "../../api";
import { useLocation, useNavigate, Link } from "react-router-dom";
import Table from "../../components/Table/Table";
import { productConstant } from "./Product.constant";
const Products = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state.product);
  const [title, setTitle] = useState("");
  const [limit, setLimit] = useState(0);
  const [page, setPage] = useState(1);

  const navigate = useNavigate();
  const location = useLocation();

  //get all products from api
  useEffect(() => {
    
    getAllProductsFromApi(limit, page, dispatch);
  }, [page, dispatch]);

  //get pagination products from api
  useEffect(() => {
 
    setLimit(6);
    getAllProductsFromApi(limit, page, dispatch);
  }, [limit, page, dispatch]);

  return (
    <div className="products">
      <h3 data-page-title>DANH SÁCH SẢN PHẨM</h3>
      <div className="products-top">
        <Button
          title={`Thêm Mới Sản Phẩm`}
          type="dark-green add-new"
          handleButtonClick={() =>
            onGoToAddNewClick({ location: location.pathname, navigate })
          }
        />
      </div>
      <div className="products-table">
        <Table
          cols={productConstant({ Link })}
          data={state.paginationProducts}
          page={page}
        />
      </div>

      <div className="products-pagination">
        <TablePagination
          paginationArr={state.products}
          limit={limit}
          page={page}
          setPage={setPage}
        />
      </div>
    </div>
  );
};

export default Products;
