export {default as Products} from './Products'
export {default as NewProduct} from './NewProduct/NewProduct'
export {default as ProductDetail} from './ProductDetail/ProductDetail'