export const changeCategories = (cat) =>{
    if (cat === "middle") return "Đặc Sản Miền Trung";
    if (cat === "north") return "Đặc Sản Miền Bắc";
    if (cat === "south") return "Đặc Sản Miền Nam";
    if (cat === "dry") return "Đồ Khô";
    if (cat === "noddle") return "Đồ Nước";
    if (cat === "vegan") return "Đồ Chay";
    if (cat === "drink") return "Đồ Uống";
    if (cat === "dessert") return "Tráng Miệng";
}