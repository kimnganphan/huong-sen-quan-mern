import { useDispatch, useSelector } from "react-redux";
import "./InputDetail.scss";
import {
  setTitle,
  setPrice,
  setDesc,
  setImg,
  setGalleryImg,
  setCategories,
} from "../../../../../store/products/product.slice";
import { useEffect, useState } from "react";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { app } from "../../../../../firebase/firebase";
import { Toast } from "../../../../../components/Toast";
import { changeCategories } from "../../ProductDetail.helper";
import { PRODUCT_CATEGORIES } from "../../../Product.constant";
const InputDetail = ({ setDisabled }) => {
  //REDUX
  const state = useSelector((state) => state.product);
  const dispatch = useDispatch();

  const [fileImg, setFileImg] = useState(null);
  const [fileArr, setFileArr] = useState(null);

  const [productCat, setProductCat] = useState([]);
  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");

  const [imgArr, setImgArr] = useState([]);
  //SET DEFAULT VALUE FOR INPUT
  useEffect(() => {
    dispatch(setTitle(state.product.title));
    dispatch(setDesc(state.product.desc));
    dispatch(setPrice(state.product.price));
    dispatch(setImg(state.product.img));
    dispatch(setGalleryImg(state.product.galleryImg));
    dispatch(setCategories(state.product.categories));
  }, [state.product, dispatch]);

  useEffect(() => {
    setImgArr(state.product.galleryImg);
  }, [state.product.galleryImg]);

  useEffect(() => {
    setProductCat(state.product.categories);
  }, [state.product.categories]);
  //HANDLE CHANGE PRODUCT THUMBNAIL IMAGE CLICK
  const handleFileChange = (e) => {
    setFileImg(e.target.files[0]);
  };

  useEffect(() => {
    if (fileImg != null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, fileImg.name);

      const uploadTask = uploadBytesResumable(storageRef, fileImg);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          // const progress =
          //   (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          // console.log("Upload is " + progress + "% done");
          // switch (snapshot.state) {
          //   case "paused":
          //     console.log("Upload is paused");
          //     break;
          //   case "running":
          //     console.log("Upload is running");
          //     break;
          //   default:
          // }
          //DISABLE BUTTON WHILE UPLOAD
          setDisabled(true);
          //INFORM TOAST
          setIsToastVisible(true);
          setToastType("info");
          setToastMessage("Ảnh Đang Được Upload. Vui Lòng Chờ...");
        },
        (error) => {
          // Handle unsuccessful uploads

          //TOAST
          setIsToastVisible(true);
          setToastType("danger");
          setToastMessage(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            //RESET BUTTON AFTER UPLOAD SUCCESSFUL
            setDisabled(false);
            //SUCCESS INFORM TOAST
            setIsToastVisible(true);
            setToastType("success");
            setToastMessage("Đã Upload Ảnh Thành Công");
            //RESET IMAGE
            setFileImg(null);
            dispatch(setImg(downloadURL));
          });
        }
      );
    }
  }, [fileImg]);

  //HANDLE CHANGE PRODUCT GALLERY IMAGE
  const handleDeleteGalleryImg = ({ e, index, item }) => {
    const newArr = [...imgArr];

    newArr.forEach((product, index) => {
      if (product === item) {
        newArr.splice(index, 1);

        setImgArr(newArr);
        dispatch(setGalleryImg(newArr));
      }
    });
  };

  const handleImgArr = (e) => {
    setFileArr(e.target.files[0]);
    e.target.value = null;
  };

  useEffect(() => {
    if (fileArr != null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, fileArr.name);

      const uploadTask = uploadBytesResumable(storageRef, fileArr);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          // const progress =
          //   (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          // console.log("Upload is " + progress + "% done");
          // switch (snapshot.state) {
          //   case "paused":
          //     console.log("Upload is paused");
          //     break;
          //   case "running":
          //     console.log("Upload is running");
          //     break;
          //   default:
          // }
          //DISABLE BUTTON WHILE UPLOAD
          setDisabled(true);
          //INFORM TOAST
          setIsToastVisible(true);
          setToastType("info");
          setToastMessage("Ảnh Đang Được Upload. Vui Lòng Chờ...");
        },
        (error) => {
          // Handle unsuccessful uploads

          //TOAST
          setIsToastVisible(true);
          setToastType("danger");
          setToastMessage(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            //RESET BUTTON AFTER UPLOAD SUCCESSFUL
            setDisabled(false);
            //SUCCESS INFORM TOAST
            setIsToastVisible(true);
            setToastType("success");
            setToastMessage("Đã Upload Ảnh Thành Công");
            //RESET IMAGE
            setFileArr(null);
            dispatch(setGalleryImg([...imgArr, downloadURL]));
            setImgArr([...imgArr, downloadURL]);
          });
        }
      );
    }
  }, [fileArr]);

  //HANDLE PRODUCT TYPE ONCHANGE EVENT
  const handleProductType = (e, type) => {
    const isChecked = productCat.includes(type);

    if (isChecked) {
      return setProductCat(productCat.filter((item) => item !== type));
    } else {
      setProductCat([...productCat, type]);
    }
  };

  useEffect(() => {
    dispatch(setCategories(productCat));
  }, [productCat, dispatch]);
  return (
    <div className="inputDetail">
      {/* --------- PRODUCT THUMBNAIL IMAGE ------ */}
      <div className="inputDetail-item">
        <label>Ảnh Đại Diện Sản Phẩm</label>
        <div className="product-image">
          <div className="image-overlay">
            <input
              type="file"
              id="file"
              style={{ display: "none" }}
              onChange={(e) => handleFileChange(e)}
            />
            <label htmlFor="file" className="edit-productImg">
              Sửa
            </label>
          </div>
          <img src={state.img} alt="" />
        </div>
      </div>

      {/* --------- END OF PRODUCT THUMBNAIL IMAGE ------- */}
      {/* ------ PRODUCT NAME ----- */}
      <div className="inputDetail-item">
        <label>Tên Sản Phẩm</label>
        <input
          type="text"
          value={state.title}
          onChange={(e) => dispatch(setTitle(e.target.value))}
        />
      </div>
      {/* ------END OF PRODUCT NAME ----- */}

      {/* ------ PRODUCT DESC ----- */}
      <div className="inputDetail-item">
        <label>Miêu Tả Sản Phẩm</label>
        <textarea
          cols="30"
          rows="10"
          value={state.desc}
          onChange={(e) => dispatch(setDesc(e.target.value))}
        ></textarea>
      </div>

      {/* ------ END OF PRODUCT DESC ----- */}

      {/* -------PRODUCT CATEGORIES ------ */}
      <div className="inputDetail-item">
        <label>Loại Sản Phẩm</label>

        <div className="inputDetail-cat">
          {PRODUCT_CATEGORIES.map((type, index) => {
            return (
              <div key={index}>
                <input
                  type="checkbox"
                  id={type}
                  checked={productCat.includes(type)}
                  onChange={(e) => handleProductType(e, type)}
                />
                <label htmlFor={type}>{changeCategories(type)}</label>
              </div>
            );
          })}
        </div>
      </div>

      {/* -------END OF PRODUCT CATEGORIES ------ */}

      {/* ------ PRODUCT PRICE ----- */}
      <div className="inputDetail-item">
        <label>Gía Sản Phẩm</label>
        <input
          type="text"
          value={state.price}
          onChange={(e) => dispatch(setPrice(e.target.value))}
        />
      </div>
      {/* ------END OF PRODUCT PRICE ----- */}

      {/* ------- PRODUCT IMAGE ARRAY ------ */}
      <div className="inputDetail-item">
        <label>Thư Viện Ảnh Sản Phẩm</label>

        <div className="productArrImage">
          <input type="file" onChange={handleImgArr} />
          <div className="productArrImage-contain">
            {imgArr?.map((item, index) => {
              return (
                <div className="productArrImage-item" key={index}>
                  <div className="image-overlay">
                    <button
                      onClick={(e) =>
                        handleDeleteGalleryImg({ e, index, item })
                      }
                    >
                      Xóa
                    </button>
                  </div>
                  <img src={item} alt="" />
                </div>
              );
            })}
          </div>
        </div>
      </div>
      {/* -------END OF PRODUCT IMAGE ARRAY ------ */}

      <Toast
        isToastVisible={isToastVisible}
        onCloseToast={() => setIsToastVisible(false)}
        type={toastType}
        message={toastMessage}
      />
    </div>
  );
};

export default InputDetail;
