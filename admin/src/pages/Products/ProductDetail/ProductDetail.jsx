import { useParams } from "react-router-dom";
import "./ProductDetail.scss";
import { getSingleProductByProductId } from "../../../api/products/productApi";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeCategories } from "./ProductDetail.helper";
import { numberWithCommas } from "../../../utils/currency.helper";
import LargeModal from "../../../components/Modal/LargeModal/LargeModal";
import { Button, onEditProductClick } from "../../../components/Button";
import { Toast } from "../../../components/Toast";
import InputDetail from "./components/InputDetail/InputDetail";
const ProductDetail = () => {
  //ROUTER
  const id = useParams();
  //REDUX
  const dispatch = useDispatch();
  const state = useSelector((state) => state.product);
  const product = useSelector((state) => state.product.product);
  const theme = useSelector((state) => state.theme.theme);
  const productList = useSelector((state) => state.product.products);
  //MODAL
  const [isVisible, setIsVisbile] = useState(false);
  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");
  //BUTTON
  const [disabled, setDisabled] = useState(false);
  const productObj = {
    ...state.product,
    galleryImg: state.galleryImg,
    img: state.img,
    title: state.title,
    price: Number(state.price),
    desc: state.desc,
    categories: state.categories
  };
  //GET PRODUCT BY PRODUCT ID
  useEffect(() => {
    getSingleProductByProductId(id.productId, dispatch);
  }, [id.productId, productList, dispatch]);

  const validateUpdateProductInfo = () => {
    const itemFound = state.products.find((item) => {
      return(item._id !== id.productId && item.title === productObj.title)
    });
    if(itemFound){
      setToastMessage("Tên Sản Phẩm Đã Tồn Tại");
      return false;
    }
    if (productObj.title.trim() === "") {
      setToastMessage("Vui Lòng Nhập Tên Sản Phẩm");
      return false;
    }
    if (productObj.desc.trim() === "") {
      setToastMessage("Vui Lòng Miêu Tả Sản Phẩm");
      return false;
    }
    if (productObj.price === 0) {
      setToastMessage("Vui Lòng Nhập Gía Sản Phẩm");
      return false;
    }
    if (productObj.price < 0 || isNaN(productObj.price)) {
      setToastMessage("Gía Sản Phẩm Không Hợp Lệ");
      return false;
    }

    if(productObj.categories.length === 0){
      setToastMessage("Vui lòng chọn loại sản phẩm");
      return false;
    }
    return true;
  };
  return (
    <div className="productDetail" data-theme={theme}>
      <h3 data-page-title>CHI TIẾT SẢN PHẨM</h3>

      <div className="productDetail-content">
        {/* ----PRODUCT THUMBNAIL IMAGE------ */}
        <div className="productDetail-content-item">
          <div className="productDetail-content-item-left">
            <label>Ảnh Đại Diện Sản Phẩm:</label>
          </div>
          <div className="productDetail-content-item-right">
            <img src={product.img} alt={product.title} />
          </div>
        </div>

        {/* ----END OF PRODUCT THUMBNAIL IMAGE------ */}

        {/* ----PRODUCT NAME------ */}
        <div className="productDetail-content-item">
          <div className="productDetail-content-item-left">
            <label>Tên Sản Phẩm:</label>
          </div>
          <div className="productDetail-content-item-right">
            <h4 data-product-title>{product.title?.toUpperCase()}</h4>
          </div>
        </div>

        {/* ----END OF PRODUCT NAME------ */}

        {/* ----PRODUCT DESC------ */}
        <div className="productDetail-content-item">
          <div className="productDetail-content-item-left">
            <label>Miêu Tả Sản Phẩm:</label>
          </div>
          <div className="productDetail-content-item-right">{product.desc}</div>
        </div>

        {/* ----END OF PRODUCT DESC------ */}

        {/* ----PRODUCT CATEGORIES------ */}
        <div className="productDetail-content-item">
          <div className="productDetail-content-item-left">
            <label>Phân Loại Sản Phẩm:</label>
          </div>
          <div className="productDetail-content-item-right">
            {product.categories?.map((cat, index) => {
              return <p key={index}>{changeCategories(cat)}</p>;
            })}
          </div>
        </div>

        {/* ----END OF PRODUCT CATEGORIES------ */}

        {/* ----PRODUCT PRICE------ */}
        <div className="productDetail-content-item">
          <div className="productDetail-content-item-left">
            <label>Gía Sản Phẩm:</label>
          </div>
          <div className="productDetail-content-item-right">
            {product.price && numberWithCommas(product.price)} VNĐ
          </div>
        </div>

        {/* ----END OF PRODUCT PRICE------ */}

        {/* ----PRODUCT GALLERY IMAGE------ */}
        <div className="productDetail-content-item">
          <div className="productDetail-content-item-left">
            <label>Ảnh Miêu Tả Sản Phẩm:</label>
          </div>
          <div className="productDetail-content-item-right">
            {product.galleryImg?.map((img, index) => {
              return <img src={img} alt={img} key={index} />;
            })}
          </div>
        </div>

        {/* ----END OF PRODUCT GALLERY IMAGE------ */}

        <div className="productDetail-content-button">
          <Button
            title="Sửa Thông Tin Sản Phẩm"
            handleButtonClick={() => setIsVisbile(true)}
            type="light"
          />
        </div>
      </div>

      <LargeModal
        isVisible={isVisible}
        onClose={() => setIsVisbile(false)}
        title="Chi Tiết Sản Phẩm"
        content={<InputDetail setDisabled={setDisabled}/>}
        footer={
          <>
            <Button
              title="Đóng"
              handleButtonClick={() => setIsVisbile(false)}
              type="red"
            />
            <Button
              disabled={disabled}
              title="Xác Nhận"
              type={`green`}
              handleButtonClick={() =>
                onEditProductClick({
                  productId: id.productId,
                  onClose: () => setIsVisbile(false),
                  dispatch,
                  product: productObj,
                  setIsToastVisible,
                  setToastType,
                  setToastMessage,
                  validateUpdateProductInfo,
                })
              }
            />
          </>
        }
      />

      <Toast
        isToastVisible={isToastVisible}
        onCloseToast={() => setIsToastVisible(false)}
        type={toastType}
        message={toastMessage}
      />
    </div>
  );
};

export default ProductDetail;
