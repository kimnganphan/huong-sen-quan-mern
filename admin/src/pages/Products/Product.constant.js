import { Button } from "../../components/Button";
const handleDeleteButtonClick = ({ setIsVisible, setProductId, id }) => {
  setIsVisible(true);
  setProductId(id);
};
export const productConstant = ({ Link }) => {
  return [
    {
      title: "STT",
      render: ({ rowData, index, page }) => {
        return <span>{index + 1 + (page - 1) * 6}</span>;
      },
    },
    {
      title: "Tên Sản Phẩm",
      render: ({ rowData }) => {
        return (
          <>
            <img src={rowData.img} alt="" />
            <span>{rowData.title}</span>
          </>
        );
      },
    },
    {
      title: "Hoạt Động",
      render: ({ rowData, setIsVisible, setProductId }) => {
        return (
          <>
            <Link to={`/products/${rowData._id}`}>
              <Button title={`Sửa`} type="red" />
            </Link>

            <Button
              title="Xóa"
              type="green"
              handleButtonClick={() =>
                handleDeleteButtonClick({
                  setIsVisible,
                  setProductId,
                  id: rowData._id,
                })
              }
            />
          </>
        );
      },
    },
  ];
};

export const PRODUCT_CATEGORIES = [
  "north",
  "south",
  "middle",
  "dry",
  "noddle",
  "vegan",
  "drink",
  "dessert",
];
