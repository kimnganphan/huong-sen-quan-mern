import "./NewProduct.scss";
import { images } from "../../../assets/images";
import { SmallModal } from "../../../components/Modal";
import { Button } from "../../../components/Button";
import { Toast } from "../../../components/Toast";

import { useEffect, useState } from "react";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { app } from "../../../firebase/firebase";
import { useDispatch, useSelector } from "react-redux";
import { createNewProduct } from "../../../api/products/productApi";
const NewProduct = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state.product);
  //PRODUCT CHECKBOX TYPE
  const productType = [
    "north",
    "south",
    "middle",
    "dry",
    "noddle",
    "vegan",
    "drink",
    "dessert",
  ];
  const renderProductType = [
    "Đặc Sản miền Bắc",
    "Đặc Sản miền Nam",
    "Đặc Sản miền Trung",
    "Đồ Khô",
    "Đồ Nước",
    "Đồ Chay",
    "Đồ Uống",
    "Tráng Miệng",
  ];
  //PRODUCT TITLE, DESC, PRICE
  const [input, setInput] = useState({});
  //PRODUCT CATEGORIES
  const [categories, setCategories] = useState([]);
  //PRODUCT IMAGE THUMBNAIL
  const [imgThumnail, setImgThumnail] = useState(null);
  const [imgSrc, setImgSrc] = useState("");
  //PRODUCT GALLERY IMAGE
  const [file, setFile] = useState(null);
  const [fileArr, setFileArr] = useState([]);
  //ALERT MODAL
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState("");
  const [modalTitle, setModalTitle] = useState("");

  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");

  //HANDLE IMAGE THUMBNAIL
  const handleImg = (e) => {
    setImgThumnail(e.target.files[0]);
    e.target.value = null;
  };
  useEffect(() => {
    if (imgThumnail != null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, imgThumnail.name);

      const uploadTask = uploadBytesResumable(storageRef, imgThumnail);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          // const progress =
          //   (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          // console.log("Upload is " + progress + "% done");
          // switch (snapshot.state) {
          //   case "paused":
          //     console.log("Upload is paused");
          //     break;
          //   case "running":
          //     console.log("Upload is running");
          //     break;
          //   default:
          // }

          //INFORM TOAST
          setIsToastVisible(true);
          setToastType("info");
          setToastMessage("Ảnh Đang Được Upload. Vui Lòng Chờ...");
        },
        (error) => {
          // Handle unsuccessful uploads
          setIsVisible(true);
          setContent(error);

          //TOAST
          setIsToastVisible(true);
          setToastType("danger");
          setToastMessage(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setImgThumnail(null);
            setImgSrc(downloadURL);

            //TOAST
            setIsToastVisible(true);
            setToastType("success");
            setToastMessage("Đã Upload Ảnh Thành Công");
          });
        }
      );
    }
  }, [imgThumnail]);

  //HANDLE ONCHANGE INPUT TEXT (PRODUCT TITLE, DESC, PRICE)
  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };
  //HANDLE IMAGE GALLERY
  const handleGallery = (e) => {
    setFile(e.target.files[0]);

    e.target.value = null;
  };
  useEffect(() => {
    if (file != null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, file.name);

      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          // const progress =
          //   (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          // console.log("Upload is " + progress + "% done");
          // switch (snapshot.state) {
          //   case "paused":
          //     console.log("Upload is paused");
          //     break;
          //   case "running":
          //     console.log("Upload is running");
          //     break;
          //   default:
          // }

          //INFORM TOAST
          setIsToastVisible(true);
          setToastType("info");
          setToastMessage("Ảnh Đang Được Upload. Vui Lòng Chờ...");
        },
        (error) => {
          // Handle unsuccessful uploads
          setIsVisible(true);
          setContent(error);

          //TOAST
          setIsToastVisible(true);
          setToastType("danger");
          setToastMessage(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setFile(null);
            setFileArr([...fileArr, downloadURL]);

            //TOAST
            setIsToastVisible(true);
            setToastType("success");
            setToastMessage("Đã Upload Ảnh Thành Công");
          });
        }
      );
    }
  }, [file]);

  //HANDLE PRODUCT TYPE
  const handleProductType = (e, type) => {
    setCategories((prev) => {
      const isChecked = categories.includes(type);

      if (isChecked) {
        return categories.filter((item) => item !== type);
      } else {
        return [...prev, type];
      }
    });
  };

  //HANDLE ADD NEW PRODUCT
  const handleAddNewProduct = (e) => {
    e.preventDefault();
    const isValidated = validateProductInfoBeforeSubmit();

    if (isValidated) {
      const product = {
        ...input,
        img: imgSrc,
        categories,
        galleryImg: fileArr,
      };

      createNewProduct({
        product: product,
        dispatch,
        resetDataAfterSubmitSuccess,
        setModalTitle,
        setContent,
        setIsVisible,
        setIsToastVisible,
        setToastType,
        setToastMessage,
      });
    } else {
      //MODAL
      setModalTitle("Cảnh Báo");
      setIsVisible(true);
      //TOAST
      setIsToastVisible(true);
      setToastType("warn");
      setToastMessage("Thiếu Dữ Liệu!");
    }
  };

  //VALIDATE PRODUCT INFOMATION
  const validateProductInfoBeforeSubmit = () => {
    //CHECK IF ALREADY CHOSE PRODUCT THUMBNAIL OR NOT
    if (imgSrc === "") {
      setContent("Vui Lòng Chọn Ảnh Sản Phẩm ");

      return false;
    }

    //CHECK IF PRODUCT TITLE ALREADY HAVE OR NOT
    const itemFound = state.products.find((item) => {
      return item.title === input.title;
    });
    if (itemFound) {
      setContent("Tên Sản Phẩm Bị Trùng");

      return false;
    }
    if (!input.title || input.title.trim() === "") {
      setContent("Vui Lòng Nhập Tên Sản Phẩm");

      return false;
    }

    //VALIDATE PRODUCT DESCRIPTION
    if (!input.desc || input.desc.trim() === "") {
      setContent("Vui Lòng Nhập Miêu Tả Sản Phẩm");

      return false;
    }

    //VALIDATE PRODUCT PRICE
    if (!input.price || input.price.trim() === "") {
      setContent("Vui Lòng Nhập Gía Sản Phẩm");

      return false;
    }

    if (isNaN(input.price) || input.price <= 0) {
      setContent("Gía Sản Phẩm Không Hợp Lệ");

      return false;
    }

    //VALIDATE PRODUCT CATEGORIES 
    if (categories.length === 0) {
      setContent("Vui Lòng Chọn Loại Sản Phẩm");

      return false;
    }
    return true;
  };

  //RESET PRODUCT DATA AFTER SUBMIT
  const resetDataAfterSubmitSuccess = () => {
    setInput({});
    setCategories([]);
    setImgSrc("");
    setFileArr([]);
  };

  return (
    <div className="newProduct">
      {/* ------------- TITLE ---------- */}
      <h3 data-page-title>THÊM MỚI SẢN PHẨM</h3>
      {/* ------------- END OF TITLE ---------- */}

      {/* ------------- CONTENT ---------- */}
      <div className="newProduct-content">
        {/* ---- PRODUCT IMAGE THUMBNAIL ----- */}
        <div className="newProduct-content-item">
          <div className="newProduct-content-item-left">
            <img
              src={imgSrc === "" ? images.noImage : imgSrc}
              alt=""
              data-thumbnail-img
            />
          </div>
          <div className="newProduct-content-item-right">
            <input type="file" name="img" onChange={(e) => handleImg(e)} />
          </div>
        </div>
        {/* ---- END OF PRODUCT IMAGE THUMBNAIL ----- */}

        {/* ---- PRODUCT NAME ----- */}
        <div className="newProduct-content-item">
          <div className="newProduct-content-item-left">
            <label>Tên Sản Phẩm</label>
          </div>
          <div className="newProduct-content-item-right">
            <input
              name="title"
              type="text"
              placeholder="Nhập Tên Sản Phẩm..."
              onChange={handleChange}
              value={input.title ? input.title : ""}
            />
          </div>
        </div>
        {/* ---- END OF PRODUCT NAME ----- */}

        {/* ---- PRODUCT DESCRIPTION ----- */}
        <div className="newProduct-content-item">
          <div className="newProduct-content-item-left">
            <label>Miêu Tả Sản Phẩm</label>
          </div>
          <div className="newProduct-content-item-right">
            <textarea
              placeholder="Nhập Miêu Tả Sản Phẩm..."
              cols="30"
              rows="10"
              name="desc"
              onChange={handleChange}
              value={input.desc ? input.desc : ""}
            ></textarea>
          </div>
        </div>
        {/* ---- END OF PRODUCT DESCRIPTION ----- */}

        {/* ---- PRODUCT PRICE ----- */}
        <div className="newProduct-content-item">
          <div className="newProduct-content-item-left">
            <label>Gía Sản Phẩm</label>
          </div>
          <div className="newProduct-content-item-right">
            <input
              onChange={handleChange}
              type="text"
              placeholder="Nhập Gía Sản Phẩm..."
              name="price"
              value={input.price ? input.price : ""}
            />
          </div>
        </div>
        {/* ---- END OF PRODUCT PRICE ----- */}

        {/* ---- PRODUCT TYPE ----- */}
        <div className="newProduct-content-item">
          <div className="newProduct-content-item-left">
            <label>Loại Sản Phẩm</label>
          </div>
          <div className="newProduct-content-item-right">
            {productType.map((type, index) => {
              return (
                <div key={index}>
                  <input
                    type="checkbox"
                    id={type}
                    checked={categories.includes(type)}
                    onChange={(e) => handleProductType(e, type)}
                  />
                  <label htmlFor={type}>{renderProductType[index]}</label>
                </div>
              );
            })}
          </div>
        </div>
        {/* ---- END OF PRODUCT TYPE ----- */}

        {/* ---- PRODUCT IMAGE GALLERY ----- */}
        <div className="newProduct-content-item">
          <div className="newProduct-content-item-left">
            <label>Thư Viện Ảnh</label>
          </div>
          <div className="newProduct-content-item-right">
            <input type="file" onChange={(e) => handleGallery(e)} />
          </div>
        </div>

        <div className="newProduct-content-item">
          {fileArr.map((file, index) => {
            return <img src={file} alt={file} key={index} data-gallery-img />;
          })}
        </div>
        {/* ---- END OF PRODUCT IMAGE GALLERY ----- */}

        {/* ---- ADD BUTTON ----- */}
        <div className="newProduct-content-item">
          <Button
            title="Thêm Sản Phẩm"
            type="light"
            handleButtonClick={handleAddNewProduct}
          />
        </div>
        {/* ---- END OF ADD BUTTON ----- */}
      </div>
      {/* -------------END OF CONTENT ---------- */}

      {/* -------- ALERT MODAL ------------ */}
      <SmallModal
        isVisible={isVisible}
        title={modalTitle}
        onClose={() => setIsVisible(false)}
        content={content}
        footer={
          <Button
            type="green"
            title="Đóng"
            handleButtonClick={() => setIsVisible(false)}
          />
        }
      />
      {/* -------- END OF ALERT MODAL ------------ */}

      {/* -------- TOAST ----------- */}
      <Toast
        isToastVisible={isToastVisible}
        onCloseToast={() => setIsToastVisible(false)}
        type={toastType}
        message={toastMessage}
      />
    </div>
  );
};

export default NewProduct;
