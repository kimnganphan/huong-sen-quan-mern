import "./Orders.scss";
import { Button, onGoToAddNewClick } from "../../components/Button";
import { useLocation, useNavigate } from "react-router-dom";
const Orders = () => {
  const navigate = useNavigate()
  const location = useLocation()
  return (
    <div className="orders">
      <h3 data-page-title>DANH SÁCH ĐƠN HÀNG</h3>
      <div className="orders-top">
        <Button
          title="Thêm Mới Đơn Hàng"
          type={`dark-green`}
          handleButtonClick={() =>
            onGoToAddNewClick({ location: location.pathname, navigate })
          }
        />
      </div>
    </div>
  );
};

export default Orders;
