import "./MainLayout.scss";
import Navbar from "./Navbar/Navbar";
import Sidebar from "./Sidebar/Sidebar";
import { useSelector } from "react-redux";
const MainLayout = ({ children }) => {
  const theme = useSelector((state) => state.theme);
  return (
    <div className="container" data-theme={theme.theme}>
      <Sidebar theme={theme.theme}/>
      <div className="content">
        <Navbar theme={theme.theme}/>
        <div className="inner">{children}</div>
      </div>
    </div>
  );
};

export default MainLayout;
