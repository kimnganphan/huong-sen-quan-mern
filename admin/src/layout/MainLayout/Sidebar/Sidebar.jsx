import { icons } from "../../../assets/icons";
import { NavLink } from "react-router-dom";
import "./Sidebar.scss";

const Sidebar = ({theme}) => {
  return (
    <div data-theme={theme} className="sidebar">
      {/* ----- LOGO ----- */}
      <div className="sidebar-logo">Admin.</div>
      {/* ----- END OF LOGO ----- */}

      {/* ----- LISTS ----- */}
      <div className="sidebar-content">
        <h5>{icons.dashIcon} Danh Mục</h5>

        <div className="sidebar-content-lists">
          <NavLink
            to="/"
            className={({ isActive }) =>
              isActive ? "activeClassName" : undefined
            }
          >
            {icons.viewIcon} Trang Chủ
          </NavLink>

          <NavLink
            to="/products"
            className={({ isActive }) =>
              isActive ? "activeClassName" : undefined
            }
          >
            {icons.storeIcon} Sản Phẩm
          </NavLink>

          <NavLink
            to="/users"
            className={({ isActive }) =>
              isActive ? "activeClassName" : undefined
            }
          >
            {icons.userIcon} Khách Hàng
          </NavLink>

          <NavLink
            to="/orders"
            className={({ isActive }) =>
              isActive ? "activeClassName" : undefined
            }
          >
            {icons.billIcon} Đơn Hàng
          </NavLink>
        </div>
      </div>
      {/* -----END OF LISTS ----- */}
    </div>
  );
};

export default Sidebar;
