import "./Navbar.scss";
import { useDispatch } from "react-redux";
import {
  chooseDarkTheme,
  choosePinkTheme,
  chooseBlueTheme,
  chooseDefault,
} from "../../../store/theme/theme.slice";

const Navbar = ({ theme }) => {
  //REDUX
  const dispatch = useDispatch();

  //HANDLE CHOOSE THEME COLOR CLICK
  const handleColor = (e, type) => {
    if (type === "pink") {
      dispatch(choosePinkTheme());
    } else if (type === "light") {
      dispatch(chooseDefault());
    } else if (type === "dark") {
      dispatch(chooseDarkTheme());
    } else {
      dispatch(chooseBlueTheme());
    }
  };

  return (
    <nav data-theme={theme}>
      <div className={`nav-wrapper`}>
        {/*------------------ THEME -----------*/}
        <div className="nav-theme">
          <div className="nav-theme-title">Chọn Theme</div>
          <div className="nav-theme-color">
            <div onClick={(e) => handleColor(e, "pink")}></div>
            <div onClick={(e) => handleColor(e, "light")}></div>
            <div onClick={(e) => handleColor(e, "dark")}></div>
            <div onClick={(e) => handleColor(e, "blue")}></div>
          </div>
        </div>
        {/*------------------END OF THEME -----------*/}
      </div>
    </nav>
  );
};

export default Navbar;
