import { icons } from "../../../assets/icons";
import "./TablePagination.scss";
import { useSelector } from "react-redux";
const TablePagination = ({ paginationArr, limit, page, setPage }) => {
  const theme = useSelector((state) => state.theme.theme);
  //HANDLE PAGINATION CLICK
  const handlePagi = (e, type, index) => {
    switch (type) {
      case "first-page":
        setPage(1);

        break;
      case "pre-page":
        setPage(page - 1);
        break;
      case "every-page":
        setPage(index);
        break;
      case "next-page":
        setPage(page + 1);
        break;
      case "last-page":
        setPage(Math.ceil(paginationArr.length / limit));
        break;
      default:
        break;
    }
  };
  return (
    <div className="table-pagination" data-theme={theme}>
      {/* ---- GO TO FIRST PAGE ----- */}
      <div
        className="table-pagination-item"
        onClick={(e) => handlePagi(e, "first-page")}
      >
        <span>{icons.doubleArrLeft}</span>
      </div>
      {/* ---- END OF GO TO FIRST PAGE ----- */}

      {/* ---- GO TO PREVIOUS PAGE ----- */}
      <div
        className="table-pagination-item"
        onClick={(e) => handlePagi(e, "pre-page")}
      >
        <span>{icons.arrLeft}</span>
      </div>
      {/* ---- END OF GO TO PREVIOUS PAGE ----- */}

      {/* ---- GO TO  PAGE ----- */}
      <div className="table-pagination-item">
        {[...Array(paginationArr.length)].map((product, index) => {
          if (index >= 1 && index <= Math.ceil(paginationArr.length / limit)) {
            return (
              <span
                key={index}
                className={page === index ? "activePage" : ""}
                onClick={(e) => handlePagi(e, "every-page", index)}
              >
                {index}
              </span>
            );
          }
        })}
      </div>
      {/* ---- END OF GO TO  PAGE ----- */}

      {/* ---- GO TO NEXT PAGE ----- */}
      <div
        className="table-pagination-item"
        onClick={(e) => handlePagi(e, "next-page")}
      >
        <span>{icons.arrRight}</span>
      </div>
      {/* ---- END OF GO TO NEXT PAGE ----- */}

      {/* ---- GO TO LAST PAGE ----- */}
      <div
        className="table-pagination-item"
        onClick={(e) => handlePagi(e, "last-page")}
      >
        <span>{icons.doubleArrRight}</span>
      </div>
      {/* ---- END OF GO TO LAST PAGE ----- */}
    </div>
  );
};

export default TablePagination;
