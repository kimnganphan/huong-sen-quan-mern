import { deleteProductByProductId, updateProductByProductId } from "../../api";
import {
  deleteUserByUserId,
  updateUserByUserId,
} from "../../api/users/userApi";

export const onGoToAddNewClick = ({ location, navigate }) => {
  if (location === "/products") {
    navigate("/products/new");
  } else if (location === "/users") {
    navigate("/users/new");
  } else if (location === "/orders") {
    navigate("/orders/new");
  }
};

export const onEditProductClick = ({
  productId,
  product,
  dispatch,
  onClose,
  setIsToastVisible,
  setToastType,
  setToastMessage,
  validateUpdateProductInfo,
}) => {
  const isValidated = validateUpdateProductInfo();
  if (isValidated) {
    updateProductByProductId({
      productId,
      product,
      dispatch,
      onClose,
      setIsToastVisible,
      setToastType,
      setToastMessage,
    });
  } else {
    setIsToastVisible(true);
    setToastType("warn");
  }
};

export const onDeleteTableDataClick = ({
  productId,
  userId,
  dispatch,
  onClose,
  setIsToastVisible,
  setToastType,
  setToastMessage,
}) => {
  productId &&
    deleteProductByProductId({
      productId,
      dispatch,
      onClose,
      setIsToastVisible,
      setToastType,
      setToastMessage,
    });
  userId && deleteUserByUserId(userId, dispatch, onClose);
};

export const onEditUserClick = ({
  userId,
  user,
  dispatch,
  onClose,
  validateUserInfoBeforeSubmit,
  setIsToastVisible,
  setToastType,
  setToastMessage,
}) => {
  const isValidated = validateUserInfoBeforeSubmit();
  if (isValidated) {
    updateUserByUserId({
      userId,
      user,
      dispatch,
      onClose,
      setIsToastVisible,
      setToastType,
      setToastMessage,
    });
  } else {
    setIsToastVisible(true);
    setToastType("warn");
  }
};
