import { Fragment } from "react";
import { useSelector } from "react-redux";
import "./Button.scss";

const Button = ({ title, type, handleButtonClick, disabled }) => {
  const theme = useSelector((state) => state.theme.theme);
 
  return (
    <Fragment>
      <button onClick={handleButtonClick} className={type} data-theme={theme} disabled={disabled}>
        {title}
      </button>
    </Fragment>
  );
};

export default Button;
