import { useEffect } from "react";
import { useState } from "react";
import { icons } from "../../assets/icons";
import "./Toast.scss";

const Toast = ({ isToastVisible = false, onCloseToast, type, message }) => {
  const [toastIcon, setToastIconType] = useState("");
  const [title, setTitle] = useState("");

  useEffect(() => {
    switch (type) {
      case "danger":
        setTitle("Lỗi");
        setToastIconType(icons.exclamation);
        break;
      case "success":
        setTitle("Thành Công");
        setToastIconType(icons.check);
        break;
      case "warn":
        setTitle("Cảnh Báo");
        setToastIconType(icons.exclamation);
        break;
      case "info":
        setTitle("Thông Tin");
        setToastIconType(icons.info);
        break;
      default:
        break;
    }
  }, [type]);
  return (
    isToastVisible && (
      <div className={`toast-container ${type}`} onClick={onCloseToast}>
        <div className="toast-top">
          <p>{icons.close}</p>
        </div>
        <div className="toast-bottom">
          {/* ------- TOAST ICON ------ */}
          <div className="toast-imageIcon">
            <span className={type}>{toastIcon}</span>
          </div>
          {/* -------END OF TOAST ICON ------ */}

          {/* ------ TOAST CONTENT ------- */}
          <div className="toast-content">
            <div className="toast-title">{title}</div>
            <div className="toast-message">{message}</div>
          </div>
          {/* ------ END OF TOAST CONTENT ------- */}
        </div>
      </div>
    )
  );
};

export default Toast;
