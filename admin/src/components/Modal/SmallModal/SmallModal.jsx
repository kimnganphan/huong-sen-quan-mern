import { useEffect } from 'react';
import './SmallModal.scss'

const SmallModal = ({isVisible = false,title, content, footer, onClose}) => {
  useEffect(() => {
    if (isVisible) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [isVisible]);
  return !isVisible ? null : (
    <div className="sm-modal" onClick={onClose}>
      <div
        className={`sm-modal-dialog`}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="sm-modal-header">
          <h3>{title}</h3>
          <span onClick={onClose}>
            &times;
          </span>
        </div>

        <div className="sm-modal-body">
          <div>{content}</div>
        </div>


        {footer && <div className="sm-modal-footer">{footer}</div>}
      </div>
    </div>
  );
}

export default SmallModal