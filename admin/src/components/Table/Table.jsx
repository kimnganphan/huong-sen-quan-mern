import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, onDeleteTableDataClick } from "../Button";
import { SmallModal } from "../Modal";
import { Toast } from "../Toast";
import "./Table.scss";

const Table = ({ cols, data, page }) => {
  const theme = useSelector((state) => state.theme.theme);
  const dispatch = useDispatch();
  //MODAL
  const [isVisible, setIsVisible] = useState(false);
  //TOAST
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastType, setToastType] = useState("");
  const [toastMessage, setToastMessage] = useState("");

  const [productId, setProductId] = useState("");
  const [userId, setUserId] = useState("");
  const [orderId, setOrderId] = useState("");

  return (
    <div className="table-responsive" data-theme={theme}>
      {/* -------- TABLE --------- */}
      <table>
        <thead>
          <tr>
            {cols.map((headerItem, index) => (
              <th key={index}>{headerItem.title}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr key={item._id}>
              {cols.map((col, key) => (
                <td key={key}>
                  {col.render({
                    rowData: item,
                    index,
                    page,
                    setProductId,
                    setIsVisible,
                    setUserId,
                  })}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      {/* ---------- END OF TABLE --------- */}

      {/* -------- MODAL --------- */}
      <SmallModal
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        title="Confirm Modal"
        content="Bạn có chắc muốn xóa dữ liệu này?"
        footer={
          <>
            <Button
              title="Đóng"
              type="red"
              handleButtonClick={() => setIsVisible(false)}
            />
            <Button
              title="Xác nhận"
              type="green"
              handleButtonClick={() =>
                onDeleteTableDataClick({
                  productId,
                  userId,
                  onClose: () => setIsVisible(false),
                  dispatch,
                  setIsToastVisible,
                  setToastType,
                  setToastMessage
                })
              }
            />
          </>
        }
      />
      {/* -------- END OF MODAL --------- */}

      {/* ------ TOAST ---------- */}
      <Toast
        isToastVisible={isToastVisible}
        onCloseToast={() => setIsToastVisible(false)}
        type={toastType}
        message={toastMessage}
      />

      {/* ------ END OF TOAST ------ */}
    </div>
  );
};

export default Table;
