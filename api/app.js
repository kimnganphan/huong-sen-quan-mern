const express = require("express");
const app = express();
const port = process.env.PORT || 5000;

const mongoose = require("mongoose");
const dotenv = require("dotenv");

const cors = require("cors");
const productRouter = require("./app/router/ProductRouter");
const userRouter = require("./app/router/UserRouter");
const cartRouter = require("./app/router/CartRouter");
const orderRouter = require('./app/router/OrderRouter')
dotenv.config();

app.use(cors());

app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);
mongoose
  .connect(process.env.MONGO_URL)
  .then(() => console.log("DB Connection Successful"))
  .catch((err) => {
    console.log(err);
  });

app.get("/", (req, res) => {
  res.json({
    message: "Huong Sen Quan Api Ready",
  });
});

app.use("/api/products", productRouter);
app.use("/api/users", userRouter);

app.use("/api/carts", cartRouter);

app.use('/api/orders', orderRouter)

app.listen(port, () => {
  console.log("Backend server is running!");
});
