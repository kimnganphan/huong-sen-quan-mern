const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      default: 123456
    },
    firstName: {
      type: String,
      required: true,
    },

    lastName: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: Number,
    },
    address: {
      type: String,
    },
    avatar: {
      type: String,
    },
  },
  { timestamps: true }
);


module.exports = mongoose.model("User", UserSchema);
