const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
    {
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      cartProducts: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Cart",
        required: true
      },
      totalAmount: {
        type:Number,
        required: true
      },
      address: {
        type: String,
        required: true
      },
      phoneNumber: {
        type:Number,
        required: true
      }, 
      status: {
        type:String,
        default: "pending"
      }
    },
    { timestamps: true }
  );
  
  module.exports = mongoose.model("Order", OrderSchema);