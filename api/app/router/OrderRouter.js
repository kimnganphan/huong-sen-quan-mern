const express = require("express");

const {
  createOrder,
  getOrderByOrderId,
  deleteOrderByOrderId,
  getAllOrders,
  getOrderByUserId
} = require("../controller/OrderController");

const router = express.Router();

//CREATE ORDER
router.post("/create-order", createOrder);

//GET ORDER BY ORDER ID
router.get("/:orderId", getOrderByOrderId);

//DELETE ORDER BY ORDER ID
router.delete("/delete-order/:orderId", deleteOrderByOrderId);

//GET ALL ORDERS
router.get("/", getAllOrders);

//GET ORDER BY USER ID
router.post("/get-user-order", getOrderByUserId)


module.exports = router;
