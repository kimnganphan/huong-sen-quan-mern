const express = require("express");

const { createUser, getAllUser, loginUser, updateUser, deleteUser, getUserById } = require("../controller/UserController");

const router = express.Router();

//CREATE USER
router.post("/", createUser)

//GET ALL USER
router.get("/", getAllUser)

//LOGIN USER
router.post("/login", loginUser)

//UPDATE USER
router.put("/:userId", updateUser)

//DELETE USER
router.delete("/:userId", deleteUser)

//GET USER BY USER ID
router.get('/:userId', getUserById)

module.exports = router;