const express = require('express');

const {createProduct, updateProduct ,deleteProduct, getAllProduct,getProductById} = require('../controller/ProductController')

const router = express.Router()

//Create Product
router.post("/", createProduct)

//Update Product By Product Id
router.put("/:productId", updateProduct)

//Delete Product By Product Id
router.delete("/:productId", deleteProduct)

//Get All Product
router.get("/", getAllProduct)

//Get Product By Product Id
router.get("/:productId", getProductById)


module.exports = router;