const express = require("express");

const {
  addProductToCart, deleteItemFromCart, updateItemFromCart, emptyItemFromCart
} = require("../controller/CartController");


const router = express.Router();

//CREATE CART
router.post("/", addProductToCart);

//DELETE ITEM FROM CART

router.post("/delete-item", deleteItemFromCart)

//UPDATE CART
router.post("/update-cart", updateItemFromCart)


//EMPTY CART ITEM
router.post("/empty-cart", emptyItemFromCart)
module.exports = router;