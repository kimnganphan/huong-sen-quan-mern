const mongoose = require("mongoose");

const User = require("../model/UserModel");

//REGISTER NEW USER
const createUser = (req, res) => {
  const newUser = new User(req.body);

  newUser
    .save()
    .then((newUser) => {
      return res.status(200).json({
        message: "Create User Success",
        user: newUser,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//GET ALL USER
const getAllUser = (req, res) => {
  const { limit, page } = req.query;
  User.find({})
    .limit(limit)
    .skip((page - 1) * limit)
    .then((userList) => {
      return res.status(200).json({
        message: "Get All User Success",
        users: userList,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//GET ONE USER
const loginUser = (req, res) => {
  User.findOne({ username: req.body.username, password: req.body.password })
    .then((user) => {
      return res.status(200).json({
        message: "Find User Success",
        user: user,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//UPDATE USER
const updateUser = (req, res) => {
  User.findByIdAndUpdate(req.params.userId, { $set: req.body }, { new: true })
    .then((updatedUser) => {
      return res.status(200).json({
        message: "Update User Success",
        updatedUser: updatedUser,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//DELETE USER
const deleteUser = (req, res) => {
  User.findByIdAndDelete(req.params.userId)
    .then(() => {
      return res.status(200).json("User Deleted Successful");
    })
    .catch((err) => {
      res.status(200).json(err);
    });
};

//GET USER BY USER ID
const getUserById = (req, res) => {
  User.findById(req.params.userId)
    .then((user) => {
      return res.status(200).json({
        message: "Get User By Id Success",
        user: user,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};
module.exports = {
  createUser,
  getAllUser,
  loginUser,
  updateUser,
  deleteUser,
  getUserById
};
