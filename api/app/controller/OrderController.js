const Order = require("../model/OrderModel");

//CREATE ORDER
const createOrder = (req, res) => {
  const newOrder = new Order(req.body);
  newOrder
    .save()
    .then((newOrder) => {
      return res.status(200).json({
        message: "Create Order Success",
        order: newOrder,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Create Order Fail",
        error: err.message,
      });
    });
};

//GET ORDER BY ORDER ID
const getOrderByOrderId = (req, res) => {
  Order.findById(req.params.orderId)
    .populate("cartProducts")
    .then((order) => {
      return res.status(200).json({
        message: "Get Order Success",
        order: order,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//DELETE ORDER BY ORDER ID
const deleteOrderByOrderId = (req, res) => {
  Order.findByIdAndDelete(req.params.orderId)
    .then(() => {
      return res.status(200).json("Order Deleted Successful");
    })
    .catch((err) => {
      res.status(200).json(err);
    });
};

//GET ALL ORDER
const getAllOrders = (req, res) => {
  Order.find({})
    .then((orderList) => {
      return res.status(200).json({
        message: "Get All Orders Success",
        orders: orderList,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//GET ORDER BY USER ID
const getOrderByUserId = (req, res) => {
  const { user } = req.body;
  Order.find({ user: user })
    .then((order) => {
      return res.status(200).json({
        message: "Get Order By User Id Success",
        order: order,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};
module.exports = {
  createOrder,
  getOrderByOrderId,
  deleteOrderByOrderId,
  getAllOrders,
  getOrderByUserId,
 
};
