const mongoose = require("mongoose");

const Cart = require("../model/CartModel");

const addProductToCart = (req, res) => {
  const { user, cartProducts } = req.body;
  Cart.findOne({ user: user })
    .populate("user")
    .populate("cartProducts.product")
    .exec((err, cart) => {
      if (err) return res.status(400).json({ err });
      if (cart) {
        //IF HAVE PRODUCT WANT TO CHECK OR ADD

        if (req.body.cartProducts) {
          req.body.cartProducts.forEach((cartProduct) => {
            const product = cartProduct.product;
            const item = cart.cartProducts.find(
              (c) => c.product._id.toString() === product
            );
            if (item) {
              // Cart.findOneAndUpdate(
              //   {
              //     user: req.body.user,
              //   },
              //   { $set: { cartProducts: cartProduct } },
              //   { new: true }
              // )
              //   .populate("user")
              //   .populate("cartProducts.product")
              //   .exec()
              //   .then((cart) => {
              //     return res.status(200).json({
              //       message: "Get Cart Success",
              //       cart: cart,
              //     });
              //   });

              item.quantity += cartProduct.quantity;
              cart.save().then((newCart) => {
                return res.status(200).json({
                  message: "Update Quantity To Cart Success",
                  cart: newCart,
                });
              });
            } else {
              // Cart.findOneAndUpdate(
              //   { user: req.body.user },
              //   { $push: { cartProducts: cartProduct } },
              //   { new: true }
              // )
              //   .populate("user")
              //   .populate("cartProducts.product")
              //   .then((cart) => {
              //     return res.status(200).json({
              //       message: "Get Cart Success",
              //       cart: cart,
              //     });
              //   });

              // let a = {product: y, quantity: cartProduct.quantiy}

              cart.cartProducts.push(cartProduct);
              cart.save().then((newCart) => {
                return res.status(200).json({
                  message: "Add Product To Cart Success",
                  cart: newCart,
                });
              });
            }
            // cart.save().then((newCart) => {
            //   return res.status(200).json({
            //     message: "Update Product Quantity To Cart Success",
            //     cart: newCart,
            //   });
            // }).catch((err)=>{
            //   return res.status(200).json({
            //     message: err.message
            //   });
            // })
          });
        } else {
          // WANT TO RETURN CART ONLY
          return res.status(200).json({
            message: "Get Cart Success",
            cart: cart,
          });
        }
      } else {
        const newCart = new Cart({
          user: user,
          cartProducts: cartProducts,
        });

        newCart
          .save()
          .then((newCart) => {
            return res.status(200).json({
              message: "Create Cart Success",
              cart: newCart,
            });
          })
          .catch((err) => {
            return res.status(500).json({
              message: "Fail",
              error: err.message,
            });
          });
      }
    });
};

const deleteItemFromCart = (req, res) => {
  const { user, productId } = req.body;
  Cart.findOne({ user: user })
    .populate("user")
    .populate("cartProducts.product")
    .exec((err, cart) => {
      if (err) return res.status(400).json({ err });
      if (cart) {
        cart.cartProducts.splice(
          cart.cartProducts.findIndex((item) => item.product._id === productId),
          1
        );

        cart.save().then((newCart) => {
          return res.status(200).json({
            message: "Delete Product  Success",
            cart: newCart,
          });
        });
      }
    });
};

const updateItemFromCart = (req, res) => {
  const { user, type, productId } = req.body;

  Cart.findOne({ user: user })
    .populate("user")
    .populate("cartProducts.product")
    .exec((err, cart) => {
      if (err) return res.status(400).json({ err });
      if (cart) {
        const item = cart.cartProducts.find(
          (c) => c.product._id.toString() === productId
        );
        if (item) {
          if (type === "inc") {
            item.quantity += 1;
            cart.save().then((cart) => {
              return res.status(200).json({
                message: "Increase Product To Cart Success",
                cart: cart,
              });
            });
          } else if (type === "desc") {
            if (item.quantity > 1) {
              item.quantity -= 1;
            }
            cart.save().then((cart) => {
              return res.status(200).json({
                message: "Descrease Product To Cart Success",
                cart: cart,
              });
            });
          }
        }
      }
    });
};

const emptyItemFromCart = (req, res) => {
  const { user } = req.body;

  Cart.findOne({ user: user }).exec((err, cart) => {
    if (cart) {
      cart.cartProducts.splice(0, cart.cartProducts.length);

      cart.save().then((cart) => {
        return res.status(200).json({
          message: "Empty Cart Success",
          cart: cart,
        });
      });
    }
  });
};
module.exports = {
  addProductToCart,
  deleteItemFromCart,
  updateItemFromCart,
  emptyItemFromCart,
};
